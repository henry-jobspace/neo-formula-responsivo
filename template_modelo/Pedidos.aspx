<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="Pedidos.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Pedidos" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
    <title>Divinitè NutriCosméticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
      
    <script src="js/jquery.1.7.js" type="text/javascript"></script>  

    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
      
    <cial:webanalytics id="include_webAnalytics" runat="server"></cial:webanalytics>
	<ciac:seo id="seo" runat="server"></ciac:seo>
</head>
<body class="main deslogado">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="header">
        <cial:header id="include_header" runat="server"></cial:header>
    </header>
	 
    <main class="box-content">
      <div class="heading-quinary">
        <h2>Meus pedidos</h2>
      </div>      
      <div class="orders">
        <div class="to-left w-68 m--w-100">
          <ciac:visualizacaopedido id="order_info" runat="server"></ciac:visualizacaopedido>
        </div>
        <div class="to-right w-30 m--w-100 m--margin-top-20">
          <ciac:visualizacaopedido id="order_items" runat="server"></ciac:visualizacaopedido>
        </div> 
      </div>
      <div class="go-back-bt align-left">
        <a href="javascript:history.go(-1);" class="bt-small bt-quaternary">Voltar</a>
      </div>
    </main>
  
    <footer id="footer">
        <cial:footer id="include_footer" runat="server"></cial:footer>
    </footer>
  
	<ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
    <div class="ajaxLoader"></div>
</form>
</body>
</html> 