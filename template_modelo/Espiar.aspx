<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Espiar.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Espiar" %><%@ Register Src="ascx/WebAnalytics.ascx" TagName="WebAnalytics" TagPrefix="ciac" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Divinit� Nutri Cosm�ticos</title>    
    <link rel="shortcut icon" href="imagens/favicon.ico">   
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>  
</head>
<body>
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<div class="quickView-product">
        <!--div class="quickView-product-product-left m--to-none m--w-100">
          <componente name="ciac:imagensproduto" id="quickView_product_images" />  
        </div-->
        <div class="quickView-product-product-right m--align-center to-right m--to-none m--w-100">
        	<div class="product-top-right-content">
              <!--componente name="ciac:produtonome" id="quickView_product_name" /-->
              <div class="product-price-container">
                <ciac:precoproduto id="quickView_product_price" runat="server"></ciac:precoproduto>
                <ciac:variantesproduto id="quickView_product_variant" runat="server"></ciac:variantesproduto>
              </div>
          	</div>
        </div>
    </div>
  	<script>
  		function AddCarrinho(ddlVar1ID, ddlVar2ID, urlDominio, qtd, templateId) {
            ddlVar1ControlID = ddlVar1ID;
            ddlVar2ControlID = ddlVar2ID;
        
            var codigo = ValidaComboVariante(ddlVar1ControlID, ddlVar2ControlID);
            if (codigo > 0) {
                var randomnumber = Math.floor(Math.random() * 1001);
                var url = urlDominio + "AddCarrinho?CodVar=" + codigo + "&Qtd=" + qtd;
                if (templateId != 0) {
                    url += '&template_id=' + templateId;
                }
      			$.colorbox.close();
                $(".ajaxLoader").show();
                $.ajax({
                    url: url
                }).done(function(){
                    $(".ajaxLoader").hide();
                    $(".header-cart-addedMessage").show("fast");
			  		setTimeout(function(){$(".header-cart-addedMessage").fadeOut()},3000);
      				$(document).trigger("atualizaCarrinhoTopo");
                });
            }
        }
  	</script>
    <ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
</form>
</body>
</html> 