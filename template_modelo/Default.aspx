<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Default.aspx.cs" MaintainScrollPositionOnPostback="true" Inherits="CiaShop.Loja.Assets.Templates._1.Default" %><%@ OutputCache Duration="1" VaryByParam="none" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/browsingHistory.ascx" TagName="browsingHistory" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
    <title>Divinit� NutriCosm�ticos</title>
  	<link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
  
    <script src="js/jquery.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
	<ciac:seo id="seo" runat="server"></ciac:seo>
  	<cial:webanalytics id="include_webanalytics" runat="server"></cial:webanalytics>
</head>
<body class="main deslogado default">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<ciac:Sorteio ID="componenteparasorteio" runat="server" />
	<header id="header">
        <cial:header id="include_header" runat="server"></cial:header>
    </header>
  
    <main style="overflow: hidden">
      <div class="banner banner-jquery">
        <ciac:banner id="banner_home_jquery" runat="server"></ciac:banner> 
      </div>
      
      <div class="beneficios-home">
        <ul class="box-content">
          <li class="beneficios-shipping">
            <p>
              <svg class="sprite-svg icon-shipping" role="img"><use xlink:href="imagens/sprite.svg#icon-shipping"></use></svg>
              <span>Entrega em todo o Brasil</span>
            </p>
          </li>
          <li class="beneficios-payment">
            <p>
              <svg class="sprite-svg icon-payment" role="img"><use xlink:href="imagens/sprite.svg#icon-payment"></use></svg>
              <span>Parcele em at� 6x sem juros</span>
            </p>
          </li>
          <li class="beneficios-shipping">
            <p>
              <svg class="sprite-svg icon-shippingBox" role="img"><use xlink:href="imagens/sprite.svg#icon-shippingBox"></use></svg>
              <span>Produtos a pronta-entrega</span>
            </p>
          </li>
          <li class="beneficios-secure">
            <p>
              <svg class="sprite-svg icon-security" role="img"><use xlink:href="imagens/sprite.svg#icon-security"></use></svg>
              <span>Compre com seguran�a</span>
            </p>
          </li>         
        </ul>
      </div>
      
      <section class="section-collections special-collection">
        <div class="box-content">
        	<div class="collections">
          		<ciac:listagemsimples id="collections_01" data_collection="collection_01" runat="server"></ciac:listagemsimples>
        	</div>
        </div>
      </section>    
      
      <section class="section-collections countdown-collection">
        <div class="box-content">
        	<div class="collections">
          		<ciac:listagemsimples id="collections_02" data_collection="collection_02" runat="server"></ciac:listagemsimples>
        	</div>
        </div>
      </section> 
      
      <!--div id="section-banners" class="banner-home-content">      
        <div class="box-content">
          <a href="/p/7/new-skin-caps" class="page-section section-banner" data-aos="fade-in" data-aos-offset="50" data-aos-delay="50" data-aos-duration="500" data-aos-easing="ease-in-out">
            <img src="imagens/banner-content-new-skin.jpg" alt="Voc� merece o melhor! Pele firme, saud�vel e bonita" title="New Skin" />
            <div class="banners-home-text">
              <h2>NEW SKIN</h2>
              <strong>Voc� merece o melhor</strong>
              <h4>Pele firme, saud�vel e bonita</h4>
              <span>Comprar</span>
            </div>
          </a>
        </div>
      </div-->
      
      <div id="section-banners" class="banner-home-content">      
        <div class="box-content">
          <a href="/p/7/new-skin-caps" class="page-section section-banner" data-aos="fade-in" data-aos-offset="50" data-aos-delay="50" data-aos-duration="500" data-aos-easing="ease-in-out">
            <img src="imagens/null.png" data-src="imagens/banner-home-content-new-skin.jpg" class="lazy" alt="Que tal levar seu NEW SKIN sempre com voc�?" title="New Skin">
            <div class="banners-home-text">
              <p>Que tal levar seu <br><strong>NEW SKIN</strong> sempre<br> com voc�?</p>
              <span>Comprar</span>
            </div>
          </a>
        </div>
      </div>

      <section class="section-collections simple-collection">
        <div class="box-content">
        	<div class="collections">
          		<ciac:listagemsimples id="collections_03" data_collection="collection_03" runat="server"></ciac:listagemsimples>
        	</div>
        </div>
      </section>        
      
      <div class="instaHome">
        <div class="box-content">
          <div class="instaHome-title">
            <svg class="sprite-svg icon-instagram" role="img"><use xlink:href="imagens/sprite.svg#icon-instagram"></use></svg>
            <h3><a href="https://www.instagram.com/divinitenutricosmeticos/" target="_blank">Divinitenutricosmeticos</a></h3>
          </div>
          <div id="instafeed"></div>
        </div>
      </div>      
      
    </main>
  
    <footer id="footer">
        <div class="box-content align-right m--align-center">
          <a href="#header" class="smoothScroll bt-toTop"><svg class="sprite-svg icon-toTop" role="img">
            <use xlink:href="imagens/sprite.svg#icon-up"></use></svg>
          </a>
      	</div>
        <cial:footer id="include_footer" runat="server"></cial:footer>
    </footer>
    
    <ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
    <div class="ajaxLoader"></div>
</form>
</body>
  
<script>
$(document).ready(function() {
    $("#banner_home_jquery").remove()
  
    $('.banner-jquery').slick({
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        autoplay: true,
  		buttons: false
    });

    $('.collection-owl').slick({
        infinite: true,
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: true,
        autoplay: false,
        responsive: [{
            breakpoint: 1366,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: true,
                dots: false
            }
        },
        {
            breakpoint: 1280,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        },
        {
            breakpoint: 840,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        }      
      ]
    });
  
    $(".special-collection .js--collection-ul").slick({
      infinite: true,
      dots: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: true,
      autoplay: true,
      buttons: false,
  	  responsive: [
        {
            breakpoint: 525,
            settings: {
                slidesToShow: 1,
            }
        }    
      ]
	});
  
  	$(".countdown-collection .js--collection-ul, .simple-collection .js--collection-ul").slick({
      infinite: true,
      dots: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      autoplay: true,
      buttons: false,
      responsive: [{
            breakpoint: 1366,
            settings: {
                slidesToShow: 4
            }
        },
        {
            breakpoint: 1280,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 840,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 525,
            settings: {
                slidesToShow: 1
            }
        }      
      ]
	});
})  
</script> 
   
<script type="text/javascript" src="js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="js/instafeed.min.js"></script>  
  
<script>
  var feed = new Instafeed({
    get: 'user',
    sortBy: 'most-recent',
    limit: 6,
    resolution: "low_resolution",
    clientId: '7ba93d4e20e143bcb416307014f2792e',
    userId: '3023230989',
    accessToken: '3023230989.7ba93d4.66d61ce8a2704acaa4359a8142ed921f',
  	template: '<a href="{{link}}" target="_blank"><img src="imagens/null.png" data-src="{{image}}" class="lazy" /></a>'
  });
  feed.run();
</script>
  
<script>
  window.onload = function(){                      
    $('#instafeed').slick({
      infinite: true,
      dots: false,
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: true,
      autoplay: true,
      responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 755,
        settings: {
          slidesToShow: 1
        }
      }      
    ]
    });
  
    $('.contador-oferta').countdown('2030/01/01', function(event) {
    	$(this).html(event.strftime(
    		'<span>%H</span> : '
    		+ '<span>%M</span> : '
    		+ '<span>%S</span>'
    	));
    });  

  }
</script>  
 
</html> 