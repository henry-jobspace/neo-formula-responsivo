<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Depato.aspx.cs" MaintainScrollPositionOnPostback="true" Inherits="CiaShop.Loja.Assets.Templates._1.Departamento" %><%@ OutputCache Duration="1" VaryByParam="departamento_id;template_id;pag" Location="Server" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><%@ Register Src="ascx/browsinghistory.ascx" TagName="browsinghistory" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
    <title>Divinit� Nutri Cosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
  
    <script src="js/jquery.1.7.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
      
	<ciac:widget id="datalayer_dpto" runat="server"></ciac:widget>  
	<ciac:seo id="seo" runat="server"></ciac:seo>
    <cial:webanalytics id="inlcude_webanalytics" runat="server"></cial:webanalytics>
</head>

<body class="main deslogado">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<ciac:Sorteio ID="componenteparasorteio" runat="server" />
	<header id="header">
        <cial:header id="include_header" runat="server"></cial:header>
    </header>

    <div class="banner js--banner-owl"></div>

    <div class="breadCrumb breadCrumb-list ifNoResult-hide">
      <div class="box-content">
      	<ciac:produtobarranavegacao id="breadCrumb" runat="server"></ciac:produtobarranavegacao>
      </div>
    </div>
  
    <main id="collection-box" class="box-content">  
          
        <aside id="collection-aside" class="m--w-100 js--aside-slider ifNoResult-hide">
            <div class="js--aside-slider-content">
              <ciac:menu id="currentDeptMenu" runat="server"></ciac:menu>
              <ciac:filtrogrupos id="departamentGroups" runat="server"></ciac:filtrogrupos>
              <ciac:filtrofaixapreco id="departamentPriceRange" runat="server"></ciac:filtrofaixapreco>
            </div>
            <div class="js--aside-slider-close hide m--show">
              <svg class="sprite-svg icon-close" role="img"><use xlink:href="imagens/sprite.svg#icon-close"></use></svg>
            </div>
        </aside>
    
        <article id="collection-content" class="m--w-100">
            <div class="banner banner-inline">
                <ciac:banner id="content_inline" runat="server"></ciac:banner> 
            </div>
            <div class="nav-filterBy ifNoResult-hide">
                <ciac:filtrosutilizados id="departamentFilterBy" runat="server"></ciac:filtrosutilizados>
            </div>
            <div class="collection">
                <ciac:listagemcompleta id="departamentCollection" runat="server"></ciac:listagemcompleta>
            </div>
        </article>
    </main>
  
    <footer id="footer">
        <div class="box-content align-right m--align-center">
          <a href="#header" class="smoothScroll bt-toTop"><svg class="sprite-svg icon-toTop" role="img">
            <use xlink:href="imagens/sprite.svg#icon-up"></use></svg>
          </a>
      	</div>
        <cial:footer id="include_footer" runat="server"></cial:footer>
    </footer>
 
    <ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
    <div class="ajaxLoader"></div>
</form>
</body>

</html> 