<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="PacotePresente.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.PacotePresente" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/topoCheckout.ascx" TagName="topoCheckout" TagPrefix="cial" %><%@ Register Src="ascx/analytics.ascx" TagName="analytics" TagPrefix="ciac" %><%@ Register Src="ascx/rodape_guest.ascx" TagName="rodape_guest" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head runat="server">
    <title>Garmin Store</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
</head>
<body>
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<div id="container">
  <div id="containertopo">
      <div class="topo" id="topo">
          <cial:topocheckout id="Topo_Check" runat="server"></cial:topocheckout>
          <div class="passos">
                <div class="seletor"><img src="imagens/passo_presente.png?24" alt="Pacote Presente" border="0"></div>    
          </div>
      </div>
  </div>

  <div class="banner banner_fulzao">
      <ciac:banner id="Fullzao_Topo" runat="server"></ciac:banner>
  </div>

  <div id="containerconteudofixo">

      <div id="conteudo">

          <div class="respiro_conteudo">


              <div class="box">
                  <ciac:pacotepresente id="PacotePresente1" runat="server"></ciac:pacotepresente>
              </div>

          </div>

      </div>
      <div id="rodape_guest">
      <cial:rodape_guest id="rodape_fluxo" runat="server"></cial:rodape_guest>
    </div>
  </div>
</div>


<ciac:analytics id="GoogleAnalytics" runat="server"></ciac:analytics>
</form>
</body>
</html>