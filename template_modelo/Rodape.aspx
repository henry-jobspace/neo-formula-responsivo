<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Rodape.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Rodape" %>
<%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %>
<%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="ciaL" %>
<link rel="stylesheet" type="text/css" href="css/geral.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<footer id="footer">
<ciaL:footer ID="footer" runat="server" />
</footer>
