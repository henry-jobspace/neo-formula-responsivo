<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Checkout.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Checkout" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/checkoutheader.ascx" TagName="checkoutheader" TagPrefix="cial" %><%@ Register Src="ascx/checkoutfooter.ascx" TagName="checkoutfooter" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head runat="server">
    <title>Divinit� Nutri Cosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
  
    <script src="js/jquery.1.7.js" type="text/javascript"></script>
    <script src="https://apis.google.com/js/platform.js" async="" defer=""></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>

	<ciac:seo id="seo" runat="server"></ciac:seo>
    <cial:webanalytics id="include_webanalytics" runat="server"></cial:webanalytics>
    <meta name="google-signin-fetch_basic_profile" content="false">
    <meta name="google-signin-scope" content="https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/user.birthday.read">
</head>
<body ng-app="appCheckout" ng-controller="checkoutController">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="checkoutHeader" class="headerCheckout" ng-class="{'step-identification' : isCustomerEditing || !hasCustomer, 'step-shipping' : isShippingEditing, 'step-payment' : isPaymentEditing}">
          <cial:checkoutheader id="include_checkoutheader" runat="server"></cial:checkoutheader>
    </header>
  
    <main class="opc box-content">
  
          <div class="heading-quinary ng-hide animate-show" ng-hide="isLoading || hasCustomer">
              <h2>Identifica��o</h2>
          </div>    
      
          <div ng-hide="isLoading || hasCustomer" class="animate-show ng-hide w-100">
            <ciac:scriptrazor id="loginCheckout" runat="server"></ciac:scriptrazor>
          </div>
    
          <div class="w-100 alert hide" ng-class="{'show':cart.hasOcurrence && hasCustomer}" id="divOrderOcurrences">
              <div class="errorCarts-ocurrenceContent error">
                	<strong>Verifique as pend�ncias do seu pedido:</strong>
                	<ul id="orderOcurrences">
                  		<li ng-repeat="ocurrence in cart.ocurrences" ng-bind-html="ocurrence.message"></li>                            
                	</ul>
              </div>
          </div>
    
          <div class="to-left w-68 m--w-100 ng-hide" ng-show="hasCustomer">
            <ciac:scriptrazor id="customer" runat="server"></ciac:scriptrazor>
            <ciac:scriptrazor id="shipping" runat="server"></ciac:scriptrazor>
            <ciac:scriptrazor id="payment" runat="server"></ciac:scriptrazor>
          </div>
      
          <div class="to-right w-30 m--w-100 align-left ng-hide" ng-show="hasCustomer">
	          <ciac:scriptrazor id="resumCart" runat="server"></ciac:scriptrazor>
          </div>      
    </main>
  
    <footer id="footer" class="clearBoth footerCheckout">
      <cial:checkoutfooter id="include_checkoutFooter" runat="server"></cial:checkoutfooter>
    </footer>
  
  <div style="display: none;">
    <div id="popup-codigo-seguranca">
      <div class="heading-primary"><h2>C�digo de Seguran�a</h2></div>
      <p class="linha">
        O que �?<br>
        O c�digo de seguran�a do cart�o de cr�dito � uma sequ�ncia num�rica complementar ao n�mero do cart�o, que garante a veracidade dos dados de uma transa��o eletr�nica, uma vez que essa informa��o � verificada somente pelo portador do cart�o e n�o consta em nenhum tipo de leitura magn�tica.
      </p>
      <div class="image-text linha">
        <div class="image">
          <img src="imagens/cartao_verso_01.png">
        </div>
        <div class="text">
          <h3>VISA / MaterCard / Diners</h3>
          <p>O c�digo de seguran�a dos cart�es com Bandeira VISA / MasterCard / Diners est� localizado no verso do cart�o e corresponde aos tr�s �ltimos d�gitos da faixa num�rica.</p>
        </div>
      </div>
      <div class="image-text linha">
        <div class="image">
          <img src="imagens/cartao_verso_02.png">
        </div>
        <div class="text">
          <h3>AMERICAN EXPRESS</h3>
          <p>O c�digo de seguran�a est� localizado na parte frontal do cart�o AMERICAN EXPRESS e corresponde aos 4 d�gitos localizados do lado direito acima da faixa num�rica do seu cart�o.</p>
        </div>
      </div>    
    </div>
  </div>

  <ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
  <div class="ajaxLoader" ng-show="isLoading"></div>
  <script type="text/javascript">
    $(function(){
    	$(document).on("click", ".cvv a", function(){
    		$.colorbox({inline:true, href:"#popup-codigo-seguranca"});
    	});
    });
  </script>
</form>
</body>
</html> 