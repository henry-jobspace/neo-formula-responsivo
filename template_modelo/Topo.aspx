<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Topo.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Topo" %>
<%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="uc1" %>
<%@ OutputCache Duration="1" VaryByParam="none" %> 
<%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="Cia" %>
<link rel="stylesheet" type="text/css" href="css/geral.css">
<link rel="stylesheet" type="text/css" href="css/header.css">
<header id="header">
<uc1:header ID="header" runat="server" />
</header>
