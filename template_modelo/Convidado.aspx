<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="Convidado.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Convidado" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/checkoutheader.ascx" TagName="checkoutheader" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><%@ Register Src="ascx/checkoutfooter.ascx" TagName="checkoutfooter" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
	<title>Divinit� Nutri Cosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
    <script src="js/jquery.1.7.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
	
	<cial:webanalytics id="inlcude_webanalytics" runat="server"></cial:webanalytics>
  	<ciac:seo id="seo" runat="server"></ciac:seo>
</head>
<body class="main deslogado">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<div id="container">
      
      <header id="checkoutHeader" class="step-identification">
          <cial:checkoutheader id="include_checkoutheader" runat="server"></cial:checkoutheader>
      </header>
      
      <main id="content" class="box-content">
        <div class="bt-medium bt-primary to-right">
          <ciac:link id="goToIdentification" runat="server"></ciac:link>
        </div>
        <div class="heading-quinary m--margin-top-30">
          <h3>Compra <strong>Express</strong> - <small>Compre sem Login e Senha</small></h3>
        </div> 
        <div class="first-buy w-60 m--w-100">
          <h2>Quer finalizar rapidamente o seu pedido mas n�o tem cadastro na loja?</h2>
          <p>Basta preencher os dados abaixo. No final da compra poder� gravar esses dados para compras futuras.</p>
        </div>
        <div class="guest form">
          <ciac:convidado id="guest" runat="server"></ciac:convidado>
        </div> 
      </main>
      <footer id="footer">
        <cial:checkoutfooter id="include_checkoutFooter" runat="server"></cial:checkoutfooter>
      </footer>
    </div>

    <ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
    <div class="ajaxLoader"></div>
    <script>
        $(function(){
            $(".form").upValidate();
        });      
    </script>
</form>
</body>
</html> 