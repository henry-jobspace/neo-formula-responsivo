<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Negado.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Negado" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/checkoutheader.ascx" TagName="checkoutheader" TagPrefix="cial" %><%@ Register Src="ascx/checkoutfooter.ascx" TagName="checkoutfooter" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
	<title>Divinit� Nutri Cosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
  
    <script src="js/jquery.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
	
	<cial:webanalytics id="inlcude_webanalytics" runat="server"></cial:webanalytics>
  	<ciac:seo id="seo" runat="server"></ciac:seo>
</head>
<body class="main deslogado">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="checkoutHeader" class="step-payment">
        <cial:checkoutheader id="include_checkoutheader" runat="server"></cial:checkoutheader>
    </header>
  
    <main class="box-content">
        <ciac:negado id="denied" runat="server"></ciac:negado>
    </main>
  
    <footer id="footer">
        <cial:checkoutfooter id="include_checkoutFooter" runat="server"></cial:checkoutfooter>
    </footer>

	<ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
</form>
</body>
</html> 