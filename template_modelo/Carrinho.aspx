<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="false" CodeBehind="Carrinho.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Carrinho" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><%@ Register Src="ascx/browsinghistory.ascx" TagName="browsinghistory" TagPrefix="cial" %><%@ Register Src="ascx/checkoutFooter.ascx" TagName="checkoutFooter" TagPrefix="cial" %><%@ Register Src="ascx/checkoutheader.ascx" TagName="checkoutheader" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">

<head runat="server">
	<title>Divinit� Nutri Cosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">

    <script src="js/jquery.1.7.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
      
	<ciac:widget id="datalayer_cart" runat="server"></ciac:widget>  
    <cial:webanalytics id="include_webAnalytics" runat="server"></cial:webanalytics>
	<ciac:seo id="seo" runat="server"></ciac:seo>
</head>

<body class="main">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="checkoutHeader" class="checkoutHeader">
          <cial:checkoutheader id="include_checkoutheader" runat="server"></cial:checkoutheader>
    </header>
   
    <div class="banner banner-full">
        <ciac:banner id="header_full" runat="server"></ciac:banner>
    </div>

    <main class="box-content align-left">     
        <ciac:carrinho id="cart" runat="server"></ciac:carrinho>
    </main>

    <footer id="footer" class="footerCheckout">
      	<cial:checkoutfooter id="include_checkoutFooter" runat="server"></cial:checkoutfooter>
    </footer>

	<ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
    <div class="ajaxLoader"></div>
</form>
</body>

</html>