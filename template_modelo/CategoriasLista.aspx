<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CategoriasLista.aspx.cs" 
    Inherits="CiaShop.Loja.Assets.Templates._1.CategoriasLista" %><%@ Register Src="ascx/Topo.ascx" TagName="Topo" TagPrefix="ciaL" %><%@ Register Src="ascx/Rodape.ascx" TagName="Rodape" TagPrefix="ciaL" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="pageHead" runat="server">
    <title>Garmin Store</title>
    <script src="js/jquery.js" type="text/javascript"></script>
</head>
<body>
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<div id="geral">

            <div id="conteudo">
                <div id="container-listas">
                   
                    <div id="menu-listas">
                     <div class="bg_listas"></div>
                    	<ciac:categoriaslista id="CategoriaLista" runat="server"></ciac:categoriaslista>
                        
                    </div>
                    <div id="conteudo-listas" class="conteudo-listas-principal">
                      <div class="categorias_lista_texto">
                        <h2>Listas Escolares</h2>
                        <p>Para a sua comodidade nesta se��o voc� encontrar� listas prontas com produtos separados por necessidades, para facilitar a sua compra e torn�-la mais �gil e eficiente.</p>
                        <p>S�o produtos selecionadas com aten��o e carinho para suprir a necessidade do seu dia a dia, sem que voc� precise busc�-los separadamente no nosso site.</p>
                        <p>Escolha a lista que mais se adeque �s suas necessidades e boas compras.</p>
                        </div>
                      <div class="categorias_lista_baixo">
                        <ul>
                          <li><img src="imagens/lista_caderno.png"><span>F�cil Acesso</span></li>
                          <li><img src="imagens/lista_globo.png"><span>F�cil Acesso</span></li>
                          <li><img src="imagens/lista_lapis.png"><span>F�cil Acesso</span></li>
                        </ul>
                      </div>
                    </div>
                </div>                
            </div>
        </div>

</form>
</body>
</html>