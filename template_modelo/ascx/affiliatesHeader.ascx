<%@ Control Language="C#"  AutoEventWireup="true" CodeBehind="Topo.ascx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.ascx.Topo" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><link href="css/afiliados.css" rel="stylesheet" type="text/css">
<div class="header-top">
  <div class="header-content">
      <div class="header-greeting">
        <ciac:labelvisitante id="greeting" runat="server"></ciac:labelvisitante>
      </div>
      <div class="header-navbar">
        <div class="header-customerData"><ciac:link id="customerData" runat="server"></ciac:link></div>        
        <div class="header-orders"><ciac:link id="linkHome" runat="server"></ciac:link></div>
      </div>
  </div>
</div>
<div class="header-middle">
  <div class="header-content">
    <div class="header-logo sprite">
      <ciac:link id="homeLogo" runat="server"></ciac:link>
    </div>
	<div class="checkoutHeader-step sprite"></div>    
  </div>
</div>
<div class="header-menu">
  <div class="header-content">
    <div class="header-menu-content">
        <ul>
            <li class="header-menu-li">
              <a href="~/afiliadoprincipal" class="header-menu-a">Home</a>
            </li>
            <li class="header-menu-li">
              <a href="~/afiliadobanners" class="header-menu-a">Op��es de Banners</a>
            </li>
            <li class="header-menu-li">
              <a href="~/afiliadorelatoriovendas" class="header-menu-a">Relat�rio de Vendas</a>
            </li>
            <!--li class="header-menu-li">
              <a href="~/afiliadorelatoriovisitas" class="header-menu-a">Relat�rio de Visitas</a>
            </li-->
            <li class="header-menu-li">
              <a href="~/afiliadodivulgaemail" class="header-menu-a">Divulga��o por E-mail</a>
            </li>
        </ul>
    </div>
  </div>
</div> 