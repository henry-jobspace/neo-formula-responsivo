<%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><div id="basket" class="mini_carrinho" ng-controller="miniCarrinho">
  
  <p class="titCart"><strong>Meu Carrinho</strong> <span class="lnr lnr-cross-circle close-minCart"></span></p>
  
  <div ng-if="miniCart.itemsCount == 0">
    <p class="miniCart-empyt">Seu carrinho de compras est� vazio.</p>
  </div>
  
  <div ng-if="miniCart.itemsCount > 0" class="miniCart-full">
    
    <div class="miniCart-content">
      
      <div class="miniCart-itens">
        <div ng-repeat="item in miniCart.items" class="contentMinicart">
          
          <div class="mini_foto">
            <a href="">
              <img ng-src="{{item.image}}?width=75&height=75" alt="">
            </a>
          </div>
          <div class="mini_titulo">
            
            <a href="">{{item.name}}</a>
            <div class="quantidade_excluir">
              <span class="qtd">Qtd: {{item.quantity}}</span>
              <span class="delete-mincart" ng-click="removeProduct(item.id)">
                <img title="Remover do carrinho" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMTZweCIgaGVpZ2h0PSIxNnB4Ij4KPGc+Cgk8Zz4KCQk8Zz4KCQkJPHBvbHlnb24gcG9pbnRzPSIzNTMuNTc0LDE3Ni41MjYgMzEzLjQ5NiwxNzUuMDU2IDMwNC44MDcsNDEyLjM0IDM0NC44ODUsNDEzLjgwNCAgICAiIGZpbGw9IiNEODAwMjciLz4KCQkJPHJlY3QgeD0iMjM1Ljk0OCIgeT0iMTc1Ljc5MSIgd2lkdGg9IjQwLjEwNCIgaGVpZ2h0PSIyMzcuMjg1IiBmaWxsPSIjRDgwMDI3Ii8+CgkJCTxwb2x5Z29uIHBvaW50cz0iMjA3LjE4Niw0MTIuMzM0IDE5OC40OTcsMTc1LjA0OSAxNTguNDE5LDE3Ni41MiAxNjcuMTA5LDQxMy44MDQgICAgIiBmaWxsPSIjRDgwMDI3Ii8+CgkJCTxwYXRoIGQ9Ik0xNy4zNzksNzYuODY3djQwLjEwNGg0MS43ODlMOTIuMzIsNDkzLjcwNkM5My4yMjksNTA0LjA1OSwxMDEuODk5LDUxMiwxMTIuMjkyLDUxMmgyODYuNzQgICAgIGMxMC4zOTQsMCwxOS4wNy03Ljk0NywxOS45NzItMTguMzAxbDMzLjE1My0zNzYuNzI4aDQyLjQ2NFY3Ni44NjdIMTcuMzc5eiBNMzgwLjY2NSw0NzEuODk2SDEzMC42NTRMOTkuNDI2LDExNi45NzFoMzEyLjQ3NCAgICAgTDM4MC42NjUsNDcxLjg5NnoiIGZpbGw9IiNEODAwMjciLz4KCQk8L2c+Cgk8L2c+CjwvZz4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMzIxLjUwNCwwSDE5MC40OTZjLTE4LjQyOCwwLTMzLjQyLDE0Ljk5Mi0zMy40MiwzMy40MnY2My40OTloNDAuMTA0VjQwLjEwNGgxMTcuNjR2NTYuODE1aDQwLjEwNFYzMy40MiAgICBDMzU0LjkyNCwxNC45OTIsMzM5LjkzMiwwLDMyMS41MDQsMHoiIGZpbGw9IiNEODAwMjciLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K">
              </span>
            </div>
            <div class="mini_preco">R$ {{item.adjustedPrice.toLocaleString('pt-BR', {minimumFractionDigits:
              2 })}}</div>
            <div class="mini_preco" ng-if="item.variant.hasInstallments == true">
              {{item.variant.quantityOfInstallmentsNoInterest}}x de
              R$ {{item.variant.valueOfInstallmentsNoInterest.toLocaleString('pt-BR',{minimumFractionDigits: 2 })}} sem juros
            </div>
            <div class="mini_preco" ng-if="item.variant.hasInstallmentsWithInterest == true">
              {{item.variant.quantityOfInstallmentsWithInterest}}x de
              R$ {{item.variant.valueOfInstallmentsWithInterest.toLocaleString('pt-BR',{minimumFractionDigits: 2 })}} sem juros
            </div>                            
          </div>
          
        </div>
        
        <p class="shippingMiniCart" ng-if="miniCart.shipToCity != null && miniCart.adjustedShippingCost != null">Frete para <b>{{miniCart.shipToCity}}</b> no valor de <b>R$ {{miniCart.adjustedShippingCost.toLocaleString('pt-BR',{minimumFractionDigits: 2 })}}</b></p>                
        
        <strong class="priceMiniCart" ng-if="miniCart.itemsCount != 0">Subtotal: <span>R$ {{miniCart.total.toLocaleString('pt-BR', { minimumFractionDigits: 2 })}}</span></strong>
      </div>
      <div class="miniCart-buttons">
        <a href="/carrinho" class="box-bt btn-secondary" target="_self">Ir para o carrinho</a>
        <a href="/checkout" class="box-bt" target="_self">Concluir compra</a>
        
      </div>
    </div>
    
    <strong class="priceMiniCart hide" ng-if="miniCart.total == 0">R$ 0,00</strong>
    
  </div>
  
  <script>
    $(document).bind('atualizaCarrinhoTopo', function () {
    	angular.element($(".mini_carrinho")).scope().incrementDataInService();
    });
    
    var app = angular.module('myApp', []);
    
    app.controller('miniCarrinho', function ($scope, $http) {
    
    $scope.incrementDataInService = function () {
    	$http({
    		method: 'GET',
    		url: "/public_api/v1/carts"
    	}).then(function successCallback(response) {
    	$scope.miniCart = response.data;
    	$(".itemsMiniCart span").text(response.data.items.length);
    	}, function errorCallback(response) {
    	});
    }
    
    $scope.incrementDataInService();
    
    $scope.removeProduct = function (taskId) {
    	$http({
    		method: 'DELETE',
    		url: "/public_api/v1/carts/items/" + taskId
    	}).then(function successCallback(response) {
    		$scope.incrementDataInService();
    	}, function errorCallback(response) {
    
    	});
    	};
    });

    angular.bootstrap(document.getElementById("basket"), ['myApp']);
    
    
  </script>
</div>