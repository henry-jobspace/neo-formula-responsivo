<%@ Control Language="C#" AutoEventWireup="true" Codebehind="Rodape.ascx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.ascx.Rodape" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><div class="footer-middle">

  <div class="footer-content box-content">
    
    <div class="footer-box w-50 m--w-100">
       <div class="footer-paymentMethods">
            <h4>Formas de pagamento</h4>
            <ul>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-visa.png" alt="Bandeira Visa" title="Bandeira Visa"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-master.png" alt="Bandeira MasterCard" title="Bandeira MasterCard"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-elo.png" alt="Bandeira Elo" title="Bandeira Elo"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-diners.png" alt="Bandeira Diners Club" title="Bandeira Diners Club"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-american.png" alt="Bandeira American Express" title="Bandeira American Express"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-boleto.png" alt="Bandeira Boleto" title="Bandeira Boleto"></span></li>              
            </ul>
        </div>    	
  	</div>
    
    <div class="footer-box w-50 m--w-100">
       <div class="footer-seals-security">
			<h4>Selos e Seguranša</h4>
            <div class="footer-seals">
                <a href="https://transparencyreport.google.com/safe-browsing/search?url=www.divinitenutricosmeticos.com.br"><img src="imagens/selo-google-safe-browsing-checkout.png" class="footer-seals-googleSafeBrowsing" alt="Google Safe Browsing" title="Google Safe Browsing"></a>
                <img src="imagens/selo-letsencrypt.svg" class="footer-seals-letsencrypt" alt="Lets Encrypt" title="Lets Encrypt">
         	</div>
            <div class="footer-poweredBy">
              <a target="_blank" href="https://www.jobspace.com.br" alt="JobSpace Creative Ideas" class="poweredBy-jobspace"></a>
              <a target="_blank" href="http://www.ciashop.com.br/" alt="Ciashop" class="poweredBy-ciashop"></a>          
            </div>
        </div>    
  	</div>
    
  </div>
</div> 