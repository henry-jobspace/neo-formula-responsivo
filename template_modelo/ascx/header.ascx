<%@ Control Language="C#"  AutoEventWireup="true" CodeBehind="s" Inherits="CiaShop.Loja.Assets.Templates._1.ascx.Topo" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><div class="header-top m--hide">
  <div class="box-content">
	<div class="header-callphone">
      <a href="tel:+554431221010" class="header-phone">
        <svg class="sprite-svg icon-phone" role="img"><use xlink:href="imagens/sprite.svg#icon-phone"></use></svg>
        <p>(44) 3122-1010</p>
      </a>
      <a href="https://api.whatsapp.com/send?l=pt&phone=5544991152002" class="header-whatsapp">
        <svg class="sprite-svg icon-whatsapp" role="img"><use xlink:href="imagens/sprite.svg#icon-whatsapp"></use></svg>
        <p>(44) 9 9115-2002</p>
      </a>
    </div>
    <div class="header-rastreioRapido">
    	<a href="https://www2.correios.com.br/sistemas/rastreamento/default.cfm" target="_blank" class="bt-rastreioRapido">
      		<svg class="sprite-svg icon-shippingExpress" role="img"><use xlink:href="imagens/sprite.svg#icon-shippingExpress"></use></svg>
      		<p>Rastreio R�pido</p>
    	</a>
  	</div>
  </div>
</div>

<div class="header-middle">
  <div class="header-content box-content">
    
      <div class="header-left hide m--show">
        <a href="javascript: void(0)" id="trigger" class="openMenu header-menu-icon"> 
            <svg class="sprite-svg icon-menu" role="img"><use xlink:href="imagens/sprite.svg#icon-menu"></use></svg>
        </a>
            
        <nav class="header-menu-fixed hide m--show">
            <ciac:menu id="menuFixed" runat="server"></ciac:menu>
        </nav>  
               
      </div>
    
      <div class="header-center">
          <div class="header-logo">
              <ciac:link id="logo" runat="server"></ciac:link>
          </div>    
      </div>
  
      <div class="header-cart-login">
        <div class="header-login">
          <ciac:loginb2b id="greetingB2B" runat="server"></ciac:loginb2b>
        </div>      
        
        <div class="header-cart js--cart-slider">    
          <div class="header-cart-icon">
            <svg class="sprite-svg icon-cart" role="img"><use xlink:href="imagens/sprite.svg#icon-cart"></use></svg>
          </div>
          <div class="header-cart-info">
            <span id="header-cart-quantity">0</span>
          </div>
          <div id="header-miniCart"></div>          
        </div>  
      </div>  
      
      <div class="header-search m--w-100">
        <ciac:caixapesquisa id="search" runat="server"></ciac:caixapesquisa>
      </div>    
    
  </div>
</div>

<div class="header-bottom m--hide">
  <div class="header-content box-content">
  	<nav class="header-menu">
    	<ciac:menu id="menu" runat="server"></ciac:menu>
  	</nav>   
  </div>
</div>


<div class="header-cart-addedMessage" style="display: none">Produto adicionado ao carrinho</div>

<div class="overlay"></div>

<div class="masKaddCart">
    <i class="spinner"></i>
</div>

<div class="modal-dialog">
    <div class="modal-head">
      <h3 id="meuModalLabel">Rastreamento de objetos</h3>
      <svg class="sprite-svg icon-close" role="img"><use xlink:href="imagens/sprite.svg#icon-close"></use></svg>
    </div>
  
	<div class="modal-body" id="modalRastreio">
      <div class="openRast"></div>
      <div id="formRastrearObj">
        <input id="rastrearObj" placeholder="Digite seu c�digo de rastreio"></input>
        <div class="bt-submitRastreamento" onclick="rastreiaCodigo($(this).prev().val())">
          <svg class="sprite-svg icon-search" role="img"><use xlink:href="imagens/sprite.svg#icon-search"></use></svg>
        </div>
        <img src="imagens/loader-small.gif">
      </div>
	</div>
</div>

<script>
$(function(){
	$(document).trigger('atualizaCarrinhoTopo');
});
</script> 