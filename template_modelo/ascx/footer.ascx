<%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><div class="footer-top"> 
  <div class="footer-content box-content">
    <div class="footer-box footer-newsletter m--w-100">
      <ciac:boletimnoticias id="newsletter" runat="server"></ciac:boletimnoticias>
    </div>
  </div>
</div>

<div class="footer-middle">
  <div class="footer-content box-content">
  
    <div class="footer-box w-25 m--w-100">
      <div class="footer-txt-list footer-institutional m--w-100">
        <h4 class="js--tab-title m--w-100" data-tab="footer-institucional">
          Institucional
          <span class="js--tab-title-icon hide m--show">
            <svg class="sprite-svg icon-add" role="img"><use xlink:href="imagens/sprite.svg#icon-toDown"></use></svg>
            <svg class="sprite-svg icon-remove" role="img"><use xlink:href="imagens/sprite.svg#icon-toTop"></use></svg>
          </span>
        </h4>
        <ul class="js--tab-content" data-tab-content="footer-institucional">
          <li><a href="~/paginainstitucional/empresa">Sobre n�s</a></li>
          <li><a href="~/paginainstitucional/termos-e-condicoes">Termos e Condi��es</a></li>          
        </ul>
      </div>
      <div class="footer-txt-list footer-help m--w-100">
        <h4 class="js--tab-title m--w-100" data-tab="footer-help">
          Ajuda e Suporte
          <span class="js--tab-title-icon hide m--show">
            <svg class="sprite-svg icon-add" role="img"><use xlink:href="imagens/sprite.svg#icon-toDown"></use></svg>
            <svg class="sprite-svg icon-remove" role="img"><use xlink:href="imagens/sprite.svg#icon-toTop"></use></svg>
          </span>
        </h4>
        <ul class="js--tab-content" data-tab-content="footer-help">
          <li><a href="~/paginainstitucional/como-comprar">Como Comprar</a></li>          
          <li><ciac:link id="contato" runat="server"></ciac:link></li>
          <!--li><componente name="ciac:link" id="duvidas" /></li-->
        </ul>
      </div>
    </div>
    
    <div class="footer-box w-25 m--w-100">
      <div class="footer-txt-list footer-categoria m--w-100">
        <h4 class="js--tab-title m--w-100" data-tab="footer-categorias">
          Categorias
          <span class="js--tab-title-icon hide m--show">
            <svg class="sprite-svg icon-add" role="img"><use xlink:href="imagens/sprite.svg#icon-toDown"></use></svg>
            <svg class="sprite-svg icon-remove" role="img"><use xlink:href="imagens/sprite.svg#icon-toTop"></use></svg>
          </span>
        </h4>
        <ul class="js--tab-content" data-tab-content="footer-categorias">
		  <li><a href="~/d/1/combos">Combos</a></li>  
          <li><a href="~/d/2/funcionalidades">Funcionalidades</a></li>  
          <li><a href="~/d/3/nutrientes">Nutrientes</a></li>  
          <li><a href="~/d/4/produtos">Produtos</a></li>  
          <li><a href="~/d/5/oferta">Ofertas</a></li>  
        </ul>
      </div>
    </div>  
  
    <div class="footer-box w-25 m--w-100">
      <div class="footer-txt-list footer-callcenter m--w-100">
        <h4 class="js--tab-title m--w-100" data-tab="footer-callcenter">
          Central de Atendimento
          <span class="js--tab-title-icon hide m--show">
            <svg class="sprite-svg icon-add" role="img"><use xlink:href="imagens/sprite.svg#icon-toDown"></use></svg>
            <svg class="sprite-svg icon-remove" role="img"><use xlink:href="imagens/sprite.svg#icon-toTop"></use></svg>
          </span>
        </h4>
        <ul class="js--tab-content" data-tab-content="footer-callcenter">
            <li>
              <a href="tel:551135943800">
                <svg class="sprite-svg icon-phone" role="img"><use xlink:href="imagens/sprite.svg#icon-phone"></use></svg>
                <span>(44) 3122-1010</span>
              </a>
            </li>
          	<li>
            	<a href="tel:551135943800">
                  	<svg class="sprite-svg icon-whatsapp" role="img"><use xlink:href="imagens/sprite.svg#icon-whatsapp"></use></svg>
                  	<span>(44) 9 9115-2002</span>
                </a>
            </li>
            <li>
            	<a href="mailto:sac@divinitenutricosmeticos.com.br">
              		<svg class="sprite-svg icon-mail" role="img"><use xlink:href="imagens/sprite.svg#icon-mail"></use></svg>
              		<span>sac@divinitenutricosmeticos.com.br</span>
              	</a>
            </li>
        </ul>
      </div>
    
      <div class="footer-txt-list footer-atendimento m--w-100">
        <h4 class="js--tab-title m--w-100">
          Hor�rio de Atendimento
          <span class="js--tab-title-icon hide m--show">
            <svg class="sprite-svg icon-add" role="img"><use xlink:href="imagens/sprite.svg#icon-toDown"></use></svg>
            <svg class="sprite-svg icon-remove" role="img"><use xlink:href="imagens/sprite.svg#icon-toTop"></use></svg>
          </span>
        </h4>
        <ul class="js--tab-content">
         	<li>
              <svg class="sprite-svg icon-clock" role="img"><use xlink:href="imagens/sprite.svg#icon-clock"></use></svg>
              <span>Seg � Sex das 8h �s 17h</span>
             </li>
        </ul>
      </div>
    </div>  
    
    <div class="footer-box w-25 m--w-100">
      <div class="footer-social-container m--w-100">
        <div class="footer-social-facebook m--hide">
          <div class="fb-page" data-href="https://www.facebook.com/divinitenutricosmeticos" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/divinitenutricosmeticos" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/divinitenutricosmeticos">Divinit� Nutricosm�ticos</a></blockquote></div>
        </div>
        <div class="footer-social">
          <a href="https://www.facebook.com/divinitenutricosmeticos"><svg class="sprite-svg icon-facebook" role="img"><use xlink:href="imagens/sprite.svg#icon-facebook"></use></svg></a>
          <a href="http://www.instagram.com/divinitenutricosmeticos"><svg class="sprite-svg icon-instagram" role="img"><use xlink:href="imagens/sprite.svg#icon-instagram"></use></svg></a>
        </div>
      </div>
    </div>  
    
  </div>
    
  <div class="footer-content box-content">
    
    <div class="footer-box w-50 m--w-100">
       <div class="footer-paymentMethods">
            <h4>Formas de pagamento</h4>
            <ul>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-visa.png" alt="Bandeira Visa" title="Bandeira Visa"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-master.png" alt="Bandeira MasterCard" title="Bandeira MasterCard"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-elo.png" alt="Bandeira Elo" title="Bandeira Elo"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-diners.png" alt="Bandeira Diners Club" title="Bandeira Diners Club"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-american.png" alt="Bandeira American Express" title="Bandeira American Express"></span></li>
              <li><span class="footer-paymentMethods-method"><img src="imagens/c-boleto.png" alt="Bandeira Boleto" title="Bandeira Boleto"></span></li>              
            </ul>
        </div>    	
  	</div>
    
    <div class="footer-box w-50 m--w-100">
       <div class="footer-seals-security">
			<h4>Selos e Seguran�a</h4>
            <div class="footer-seals">
                <a href="https://transparencyreport.google.com/safe-browsing/search?url=www.divinitenutricosmeticos.com.br"><img src="imagens/selo-google-safe-browsing.png" class="footer-seals-googleSafeBrowsing" alt="Google Safe Browsing" title="Google Safe Browsing"></a>
                <img src="imagens/selo-letsencrypt.svg" class="footer-seals-letsencrypt" alt="Lets Encrypt" title="Lets Encrypt">
         	</div>
            <div class="footer-poweredBy">
              <a target="_blank" href="https://www.jobspace.com.br" alt="JobSpace Creative Ideas" class="poweredBy-jobspace"></a>
              <a target="_blank" href="http://www.ciashop.com.br/" alt="Ciashop" class="poweredBy-ciashop"></a>          
            </div>
        </div>    
  	</div>
    
  </div>
</div>

<div class="footer-text m--align-center">
  <div class="box-content">
    <div class="footer-box w-100 m--w-100">
      <div class="footer-text-copyright">
        <p>Raz�o Social: Nutracosm�ticos do Brasil Industria e Com�rcio de Suplementos LTDA</p>
        <p>CNPJ: 21.449.177/0001-40</p>
		<h4>� 2019 Divinit� Nutricosm�ticos. Todos os direitos reservados.</h4>
      </div>
    </div>
  </div>
</div>

<div id="fb-root"></div>
<script async="" defer="" crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.2&autoLogAppEvents=1"></script> 