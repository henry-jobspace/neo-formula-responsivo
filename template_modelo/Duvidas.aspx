<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="Duvidas.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.PaginaInstitucional" EnableEventValidation="false" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
    <title>Divinit� Nutri Cosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
    <script src="js/jquery.1.7.js" type="text/javascript"></script>
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
    <cial:webanalytics id="include_webAnalytics" runat="server"></cial:webanalytics>
	<ciac:seo id="seo" runat="server"></ciac:seo>
</head>
<body class="main deslogado">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="header">
        <cial:header id="include_header" runat="server"></cial:header>
    </header>
  

    <div class="banner banner-full">
        <ciac:banner id="header_full" runat="server"></ciac:banner>
    </div>
    	
  	<main class="box-content">
        <div class="heading-quinary">
            <h3>D�vidas</h3>
        </div>      
      	<div>
        	<ciac:duvidas id="faq" runat="server"></ciac:duvidas>	
          	<ciac:faleconosco id="contactUsForm" runat="server"></ciac:faleconosco>
      	</div>
  	</main>
    
  	<footer id="footer">
    	<cial:footer id="include_footer" runat="server"></cial:footer>
    </footer>
       
    <ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>	
    
  	<script>
      $(document).ready(function(){
        $(".form").upValidate();
      });
    </script>
    
  	<div class="ajaxLoader"></div>
</form>
</body>
</html> 