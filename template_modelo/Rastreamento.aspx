<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="FaleConosco.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.FaleConosco" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
  
<head runat="server">
	<title>Garmin Store</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
  
    <script src="js/jquery.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
      
	<ciac:seo id="seo" runat="server"></ciac:seo>
	
	<cial:webanalytics id="include_webanalytics" runat="server"></cial:webanalytics>
  </head>
<body>
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<div id="container">
		<header id="header">
			<cial:header id="include_header" runat="server"></cial:header>
		</header>  
      	<hr class="header-division">
		<div id="content">
			<main><div id="col-content">
              	<ciac:scriptrazor id="orderTracking" runat="server"></ciac:scriptrazor>
            </div></main>
		</div>
		<footer id="footer">
			<cial:footer id="include_footer" runat="server"></cial:footer>
		</footer>
	</div>
     
    <ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
    <div class="ajaxLoader"></div>
	<ciac:widget id="upSearch" runat="server"></ciac:widget>
</form>
</body>
</html>