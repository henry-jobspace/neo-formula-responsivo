<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Lista.aspx.cs" MaintainScrollPositionOnPostback="true" Inherits="CiaShop.Loja.Assets.Templates._1.Lista" %><%@ OutputCache Duration="1" VaryByParam="departamento_id;template_id;pag" Location="Server" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><%@ Register Src="ascx/browsinghistory.ascx" TagName="browsinghistory" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
    <title>Divinit� Nutri Cosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
  
    <script src="js/jquery.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
      
	<ciac:seo id="seo" runat="server"></ciac:seo>
    <cial:webanalytics id="inlcude_webanalytics" runat="server"></cial:webanalytics>
</head>
<body class="main deslogado">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="header">
        <cial:header id="include_header" runat="server"></cial:header>
    </header>
  
    <div class="breadCrumb breadCrumb-list ifNoResult-hide">
      <div class="box-content">
      	<ciac:produtobarranavegacao id="breadCrumb" runat="server"></ciac:produtobarranavegacao>
      </div>
    </div>  
    
    <div class="banner js--banner-owl"></div>
  
    <main id="collection-box" class="box-content">  
        <aside id="collection-aside" class="m--w-100 js--aside-slider ifNoResult-hide">
            <div class="js--aside-slider-content">
              <ciac:filtrodepartamentos id="listDepartaments" runat="server"></ciac:filtrodepartamentos>
              <ciac:filtrogrupos id="listGroups" runat="server"></ciac:filtrogrupos>
              <ciac:filtrofaixapreco id="listPriceRange" runat="server"></ciac:filtrofaixapreco>
            </div>
            <div class="js--aside-slider-close hide m--show">
              <svg class="sprite-svg icon-close" role="img"><use xlink:href="imagens/sprite.svg#icon-close"></use></svg>
            </div>
        </aside>
    
        <article id="collection-content" class="m--w-100">
            <div class="banner banner-inline">
                <ciac:banner id="content_inline" runat="server"></ciac:banner>
            </div>
            <div class="nav-filterBy ifNoResult-hide">
                <ciac:filtrosutilizados id="listFilterBy" runat="server"></ciac:filtrosutilizados>
            </div>
            <div class="collection">
                <ciac:listagemcompleta id="listCollection" runat="server"></ciac:listagemcompleta>
            </div>
        </article>
    </main>

    <footer id="footer">
        <div class="box-content align-right m--align-center">
          <a href="#header" class="smoothScroll bt-toTop"><svg class="sprite-svg icon-toTop" role="img">
            <use xlink:href="imagens/sprite.svg#icon-up"></use></svg>
          </a>
      	</div>
        <cial:footer id="include_footer" runat="server"></cial:footer>
    </footer>

  	<ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
    <div class="ajaxLoader"></div>
</form>
</body>
</html> 