<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImagensProduto.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.ImagensProduto" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Divinit� Nutri Cosm�ticos</title>
</head>
<body>

	<ciac:imagensproduto id="quickView_product_images" runat="server"></ciac:imagensproduto>
    <script>
       $(function(){
      
            $(".product-image-small .product-image-repeat:first-child img").addClass("active");
            $(".product-image-small img").click(function(){
                $(".product-image-small img").removeClass("active");
                $(this).addClass("active");
            });
      
            $(".product-image-big img").attr('src',$(".product-image-big img").attr('src').replace('Gigantes','SuperZoom'));
            $('.product-image-repeat img').each(function(){
                $(this).attr('onclick',$(this).attr('onclick').replace('Gigantes','SuperZoom'));
            });
      
       });
    </script>

</body>
</html> 