<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProdutoDetalhe.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.ProdutoDetalhe" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/Analytics.ascx" TagName="Analytics" TagPrefix="ciac" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
   <head runat="server">
      <meta name="viewport" content="width=device-width, user-scalable=no">
      <title>Provador inteligente</title>
      <link rel="shortcut icon" href="imagens/favicon.ico">
      <script src="https://code.jquery.com/jquery-1.7.0.min.js" crossorigin="anonymous"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
      <link rel="stylesheet" href="css/provador.css">
      <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400" rel="stylesheet">
      <script src="js/js.cookie.js"></script>
      <script src="js/jquery.colorbox-min.js" type="text/javascript"></script>
      <link href="css/jquery.jqzoom.custom.css" rel="stylesheet" type="text/css">
      <script src="js/jquery.mask.js"></script>
      <link type="text/css" href="css/colorbox.css" rel="stylesheet">
      <link type="text/css" href="css/jquery.keypad.css" rel="stylesheet">
      <script type="text/javascript" src="js/jquery.plugin_keypad.js"></script> 
      <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
      <script type="text/javascript">
         //$( document ).ready(function() { loadjs(); });
      </script>
   </head>
   <!-- atributo NG-APP inicia a aplica��o, tudo que utilizarmos a linguagem Angular deve ser feita dentro da tag com essa diretiva. -->
   <body ng-app="appProvador" class="main" onselectstart="return false" oncontextmenu="return false" ondragstart="return false" onmouseover="window.status='..message perso .. '; return true;">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<div ng-controller="provadorController" ng-init="secToReloadScreen=5; minToRestScreen=2; secHelpDisplay=10;">
         <!-- **** TELA 01 - BEM-VINDO **** -->
         <div class="ng-hide" ng-hide="!cart || started">
            <section ng-hide="cart.customer" class="provador-bemVindo">
               <h1>
                  <strong>Bem-vindo</strong>
               </h1>
            </section>
            <section ng-show="cart.customer" class="provador-bemVindo" ng-click="startInteraction();">
               <h1>Ol�,
                  <strong>{{cart.customer.name}}</strong>
               </h1>
               <span class="provador-iniciarInteracao">Toque para interagir
               </span>
            </section>
         </div>
         <!-- -->
         <!--
            **** TELA 02 - EXIBI��O DOS PRODUTOS ****
            > section com class 'provador-roupaSelecioda' mostra o lado esquerdo, com produto selecionado
            > section com class 'provador-carrinho' mostra lado diretio, com produto que est�o no provador.
            -->
         <div class="ng-hide" ng-show="started">
            <section class="provador-roupaSelecionada" ng-class="{'showRoupa': selectedItem != 'null', 'hideRoupa': selectedItem == null}">
               <span class="btn-deslizeFechar" ng-click="selectedItem.select()">fechar</span>
               <h1>{{selectedItem.variant.name}}</h1>
               <aside class="provador-produtoImagens carrossel">
                  <a href="#" ng-click="prev()" ng-show="selectedItem.variant.variantImages.length > 1" class="btnPrev">prev</a>
                  <img ng-repeat="image in selectedItem.variant.variantImages" ng-src="{{image.imagePath}}?width=500&height=500" alt="{{image.sortOrder}} - {{selectedItem.variant.product.name}}" ng-show="iVariant==image.index" width="500" height="500">
                  <a href="#" ng-click="next()" ng-show="selectedItem.variant.variantImages.length > 1" class="btnNext">next</a>
                  <ul class="bullets" ng-show="selectedItem.variant.variantImages.length > 1">
                     <li ng-repeat="image2 in selectedItem.variant.variantImages" ng-class="{'selected': iVariant==image2.index}">
                        <a href="#" ng-click="goto(image2.index);">�</a>
                     </li>
                  </ul>
               </aside>
               <aside class="provador-infoProduto">
                  <a href="#" ng-show="selectedItem.isInCart" class="btn-noProvador">No provador</a>
                  <aside ng-hide="selectedItem.isInCart">
                     <a href="#" class="btn-queroProvar" ng-hide="selectedItem.variant.requestedFromSeller" ng-click="selectedItem.requestFromSeller()">Quero provar esta pe�a</a>
                     <a href="#" class="btn-solicitada" ng-show="selectedItem.variant.requestedFromSeller">Pe�a solicitada ao vendedor</a>
                  </aside>
               </aside>
               <aside class="provador-variantesProduto">
                  <div class="provador-variante01">
                    <div style="width:{{widthProvadorVariante01?widthProvadorVariante01:1000}}px;">
                     <a href="#" class="variantText" ng-repeat="definition in selectedItem.variant.product.definition1Values" ng-click="definition.select()" ng-class="{'selected': definition.selected, 'variantImage': selectedItem.variant.product.definition1TypeOfDisplay == 'image'}">
                     <span ng-if="!definition.image">{{definition.value}}</span>
                     <img ng-if="definition.image" ng-src="{{definition.image}}" alt="{{definition.value}}">
                     </a>
                    </div>
                  </div>
                  <div class="provador-variante02">
                    <div style="width:{{widthProvadorVariante02?widthProvadorVariante02:1000}}px;">
                     <a href="#" class="variantText" ng-repeat="definition in selectedItem.variant.product.definition2Values" ng-click="definition.select()" ng-class="{'selected': definition.selected, 'variantImage': selectedItem.variant.product.definition2TypeOfDisplay == 'image'}">
                     <span ng-if="!definition.image">{{definition.value}}</span>
                     <img ng-if="definition.image" ng-src="{{definition.image}}" alt="{{definition.value}}">
                     </a>
                    </div>
                  </div>
               </aside>
               <aside class="provador-relacionados" ng-show="relatedProducts">
                  <h3 class="provador-tituloRelacionados">Complete seu look</h3>
                  <div class="provador-vitrineRelacionados">
                    <div style="width:{{widthProvadorRelacionados?widthProvadorRelacionados:1000}}px;">
                     <a href="#" class="selected2" ng-class="{'selected': relatedProduct.item.selected}" ng-click="relatedProduct.item.select();" ng-repeat="relatedProduct in relatedProducts">
                     <img ng-src="{{relatedProduct.image}}" alt="{{relatedProduct.name}}" style="height: 130px;"></a>
                    </div>
                  </div>
               </aside>
            </section>
            <section class="provador-carrinho" ng-class="{'selectedItemLeft': selectedItem != 'null', 'noSelectedItemLeft': selectedItem == null}">
               <aside class="provador-acessoRapido">
                  <aside class="ligth" ng-show="ligth.isLigthDisplay">
                     <u>
                        <li ng-repeat="option in ligth.options" ng-class="{'selected': option.selected}">
                          <a href="" ng-click="option.select()" ng-class="{ 'default': option.name == 'Padr�o', 'sol': option.name == 'Sol', 'entardecer': option.name == 'Entardecer', 'noite': option.name== 'Noite', 'Fechar': option.name=='Fechar' }">
                           {{option.name}}</a>
                        </li>
                     </u>
                  </aside>
                  <div class="saudacao">
                     <h1 ng-show="cart.customer.name">Ol�,<br>
                        {{cart.customer.firstName}}
                     </h1>
                     <h1 ng-hide="cart.customer.name"><br>
                        Bem-vindo
                     </h1>
                  </div>
                  <aside class="help" ng-show="isHelpDisplay">
                     <h2>Sua ajuda chegar� em breve</h2>
                  </aside>
                  <div class="iluminicaoAjuda">
                     <a href="" ng-click="ligth.select()">
                     <img src="imagens/icoTrocar.png" alt="">
                     Trocar ilumini��o
                     </a>
                     <a href="" ng-click="requestHelp()">
                     <img src="imagens/icoAjuda.png" alt="Solicitar ajuda">
                     Solicitar ajuda
                     </a>						
                  </div>
               </aside>
               <section class="provador-listaProdutos">
                  <aside class="provador-listaProdutos-titulo">
                     <h2 ng-show="cart.itemsCount<=1">{{cart.itemsCount}} pe�a no provador</h2>
                     <h2 ng-show="cart.itemsCount>1">{{cart.itemsCount}} pe�as no provador</h2>
                     <h3>Clique nas pe�as para mais informa��es</h3>
                  </aside>
                  <aside class="provador-listaProdutos-produtos">
                     <!-- items -->
                     <div ng-repeat="item in cart.items" class="provador-listaProdutos-boxProduto" ng-class="{'selected': item.selected}" ng-click="item.select();">
                        <div class="provador-listaProdutos-imgProduto">
                           <img ng-src="{{item.image}}" alt="{{item.name}}" width="125" height="125">
                        </div>
                        <div class="provador-listaProdutos-infoProduto">
                           <h4>{{item.name}}</h4>
                           <div class="variantes">
                              <span ng-if="item.variant.definition1Value && !item.variant.definition1ImagePath">{{item.variant.definition1Value}}</span>
                              <span ng-if="item.variant.definition1ImagePath"><img ng-src="{{item.variant.definition1ImagePath}}" alt=""></span>
                              <span ng-if="item.variant.definition2Value">{{item.variant.definition2Value}}</span>
                              <span ng-if="item.variant.definition2ImagePath && !item.variant.definition2ImagePath"><img ng-src="{{item.variant.definition2ImagePath}}" alt=""></span>
                           </div>
                        </div>
                     </div>
                     <!-- -->
                  </aside>
               </section>
            </section>
         </div>
         <!-- -->
         <!-- :: Bloqueio de Fundo :: -->
		 <div ng-show="selectedItem.loadingProduct || selectedItem.loadingVariantImages">
			<div class="shadow"></div>
		 </div>
         <!-- -->
		 
         <!-- :: LOADER :: -->
         <!-- Atributo ng-show vai utilizar a fun��o isLoading que � respons�vel por verificar se a p�gina est� sendo carregada -->
         <!--div class="load" ng-show="isLoading" class="animate-show main-container">
            <div class="loader-onePage hide">
               <span>Sincronizando...</span>
               <img src="https://sprint371.myciashop.local/assets/templates/160/imagens/giphy.gif" height="50">
            </div>
         </div -->
		 
         <!-- :: MODIFICACAO NO CARRINHO :: -->
         <!--div class="cart-changed hide" ng-show="isCartChanged && (newItems.length || deletedItems.length)" class="animate-show main-container" ng-click="isCartChanged=false">
			<div class="logo">
				<img src="https://sprint371.myciashop.local/assets/templates/160/imagens/giphy.gif" style="/* height: 40px; *//* display: inline-block; */">
			</div>
            <div class="notification">
               <span>O carrinho mudou!</span>					
               <aside class="provador-listaProdutos-produtos" ng-show="newItems.length">
                  + Adicionados:
                  < !-- newItems -- >
                  <div ng-repeat="item in newItems" class="provador-listaProdutos-boxProduto">
                     <div class="provador-listaProdutos-infoProduto">
                        <span>+ {{item.name}}</span>
                     </div>
                  </div>
                  < !-- -- >
               </aside>
               <aside class="provador-listaProdutos-produtos" ng-show="deletedItems.length">
                  - Removidos:
                  < !-- newItems -- >
                  <div ng-repeat="item in deletedItems" class="provador-listaProdutos-boxProduto">
                     <div class="provador-listaProdutos-infoProduto">
                        <span>- {{item.name}}</span>
                     </div>
                  </div>
                  < !-- -- >
               </aside>
            </div>
         </div-->
		 
      </div>
      <!-- Grava cookie com origem mesa -->
      <div style="display:none;">
         <div id="idMesa">
            <p>Informe o nome do provador:</p>
            <textarea id="inputNomeComprador" rows="1" cols="6" class="nomeMesa" maxlenght="50"></textarea>
            <ul class="defaultKey keyBoardNameModal">
               <li class="letter">1</li>
               <li class="letter">2</li>
               <li class="letter">3</li>
               <li class="letter">4</li>
               <li class="letter">5</li>
               <li class="letter">6</li>
               <li class="letter">7</li>
               <li class="letter">8</li>
               <li class="letter">9</li>
               <li class="letter lastitem">0</li>
               <li class="letter">q</li>
               <li class="letter">w</li>
               <li class="letter">e</li>
               <li class="letter">r</li>
               <li class="letter">t</li>
               <li class="letter">y</li>
               <li class="letter">u</li>
               <li class="letter">i</li>
               <li class="letter">o</li>
               <li class="letter">p</li>
               <li class="letter">a</li>
               <li class="letter">s</li>
               <li class="letter">d</li>
               <li class="letter">f</li>
               <li class="letter">g</li>
               <li class="letter">h</li>
               <li class="letter">j</li>
               <li class="letter">k</li>
               <li class="letter">l</li>
               <li class="letter">�</li>
               <li class="capslock">caps</li>
               <li class="letter">z</li>
               <li class="letter">x</li>
               <li class="letter">c</li>
               <li class="letter">v</li>
               <li class="letter">b</li>
               <li class="letter">n</li>
               <li class="letter">m</li>
               <li class="delete lastitem"><img src="imagens/deleteKey.png" style="vertical-align: -3px"></li>
               <li class="space lastitem">&nbsp;</li>
            </ul>
            <a href="javascript:void(0)" id="gravaIdMesa">gravar provador</a>
         </div>
      </div>
     <style>
       #cboxClose{display:none !important;}
     </style>
      <script>
          $(function () {
              if (Cookies.get('origemComprador') == null) {
                  $.colorbox({ width: "auto", inline: true, href: "#idMesa", overlayClose: false, closeButton: false });
                  $('#gravaIdMesa').click(function () {
                      var idMesa = $(".nomeMesa").val();
                      Cookies.set('origemComprador', idMesa, { expires: 365 * 10 });
                      $('#idMesa').colorbox.close();
                      window.location.reload();
                  })
              }

          })
      </script>
      <script>
          $(document).ready(function () {
              $(function () {
                  var $boardName = $('.nomeMesa'),
                      shift = false,
                      capslock = false;

                  $('.keyBoardNameModal li').click(function () {
                      var $this = $(this),
                          character = $this.html(); // If it's a lowercase letter, nothing happens to this variable

                      // Shift keys
                      if ($this.hasClass('left-shift') || $this.hasClass('right-shift')) {
                          $('.letter').toggleClass('uppercase');
                          $('.symbol span').toggle();

                          shift = (shift === true) ? false : true;
                          capslock = false;
                          return false;
                      }

                      // Caps lock
                      if ($this.hasClass('capslock')) {
                          $('.letter').toggleClass('uppercase');
                          capslock = true;
                          return false;
                      }

                      // Delete
                      if ($this.hasClass('delete')) {
                          var html = $boardName.html();

                          $boardName.html(html.substr(0, html.length - 1));
                          return false;
                      }

                      // Special characters
                      if ($this.hasClass('symbol')) character = $('span:visible', $this).html();
                      if ($this.hasClass('space')) character = ' ';
                      if ($this.hasClass('tab')) character = "\t";
                      if ($this.hasClass('return')) character = "\n";

                      // Uppercase letter
                      if ($this.hasClass('uppercase')) character = character.toUpperCase();

                      // Remove shift once a key is clicked.
                      if (shift === true) {
                          $('.symbol span').toggle();
                          if (capslock === false) $('.letter').toggleClass('uppercase');

                          shift = false;
                      }

                      // Add the character
                      $boardName.html($boardName.html() + character);
                  });
              });


          })

      </script>
</form>
</body>
</html> 