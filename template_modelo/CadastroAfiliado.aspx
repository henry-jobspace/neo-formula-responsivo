<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CadastroAfiliado.aspx.cs" MaintainScrollPositionOnPostback="true" Inherits="CiaShop.Loja.Assets.Templates._1.CadastroAfiliado" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/affiliatesheader.ascx" TagName="affiliatesheader" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head runat="server">
    <title>Cadastro Afiliados</title>
    <script src="js/jquery.1.7.js" type="text/javascript"></script>
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
  
<style>
.header-top, .header-menu{
  display: none;
}  
  
body #header{
  height: initial;
}
</style>  
</head>
<body>
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="header" class="step-bank">
        <cial:affiliatesheader id="include_affiliatesHeader" runat="server"></cial:affiliatesheader>
    </header>

    <div id="containerconteudo" class="containerconteudo_fixo">
        <div id="conteudo">
           <ciac:cadastrodeafiliado id="CadastroDeAfiliado" runat="server"></ciac:cadastrodeafiliado>
        </div>
    </div>
  
  <footer id="footer">
      <cial:footer id="include_footer" runat="server"></cial:footer>
  </footer>

<ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
</form>
</body>
</html>