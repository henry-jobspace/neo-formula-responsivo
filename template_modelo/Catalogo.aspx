<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Boletim.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Boletim" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head runat="server">
    <title>Garmin Store</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <script type="text/javascript" src="js/jquery.js"></script>
  
  	<ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
  	<style>
  		#header{
        	position: static;
        	height: 80px;
        }
      	body .header-middle{
      		height: 70px;
      	}
      	body .header-cart{
      		top: 8px;
      	}
      	body .header-logo{
      		background-position: 0 -360px;
      		top: 7px;
      		width: 182px;
      		height: 55px;
      	}
      	body 
  	</style>
    <script>
    var exibirVar2LadoALado = false;
    var  document_ready_listagem = true;
    var variante = [[]];
    function AbrirModalEspiar(urlDominio, codigo, template) {
      		template = 1;
            var url = urlDominio + 'Espiar.aspx?produto_id=' + codigo + '&source=es';
            if (template != 0) {
                url += '&template_id=' + template;
            }
            $.colorbox({ href: url,  innerWidth:900, scrolling: false, onComplete: function () {
            	if (($('.jqzoom').length > 0)) {
            		$('.jqzoom').jqzoom({ zoomType: 'innerzoom', lens: true, title: false, preloadImages: false, alwaysOn: false, preloadText: 'Carregando...' }); 
            	}
      			if($(".variant-1 a").length > 7){
      				$(".variant-1").owlCarousel({navigation:true, items: 7, scrollPerPage: true, slideSpeed: 800, pagination: false,itemsDesktop: false, itemsDesktopSmall: false, itemsTablet: false, itemsMobile: false});
      			}
      			if($(".variant-2 a").length > 7){
      				$(".variant-2").owlCarousel({navigation:true, items: 7, scrollPerPage: true, slideSpeed: 800, pagination: false,itemsDesktop: false, itemsDesktopSmall: false, itemsTablet: false, itemsMobile: false});
      			}
            }	
            });
    }
    </script>
    <cial:webanalytics id="inlcude_webanalytics" runat="server"></cial:webanalytics>
</head>
<body style="overflow: hidden;">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<div id="container">
      
      <div id="header" data-element="header">
        <div class="header-middle">
          <div class="header-content">
            <div class="header-logo sprite">
              <ciac:link id="homeLogo" runat="server"></ciac:link>
            </div>
            <div class="header-cart arrow-bottom">    
              <div class="header-cart-icon sprite"></div>
              <ciac:link id="cart" runat="server"></ciac:link>
              <div class="header-cart-info">
                  <span id="header-cart-quantity">0 Itens</span>
              </div>
              <div id="header-miniCart"></div>
            </div>
          </div>
        </div>
      </div>
      <script>
        function loadMiniCart(){
            var cartQuantity = 0;
            $.ajax({
              url: "//" + window.location.hostname + "/public_api/v1/carts/minicart",
              dataType: "json",
                cache: false,
              xhrFields: {
                withCredentials: true
              },
              success: function (data){
                html = '<div class="header-cart-items float-box float-box-right">';
                if(data.items.length>0){
                  cartQuantity = data.items.length;
                  html = html+'<div class="header-cart-items-list"><table cellpadding="0" cellspacing="0">';
                  $.each(data.items, function (index, item) {
                    html = html+'<tr>'
                    html = html+'<td class="header-cart-item-image"><a href="'+item.url+'"><img src="'+item.image+'?width=50&height=50" alt="" /></a></td>'
                    html = html+'<td class="header-cart-item-name"><a href="'+item.url+'">'+item.name+'</a></td>'
                    html = html+'<td><span class="header-cart-item-quantity">'+item.quantity+' un.</span><span class="header-cart-item-price">R$'+item.adjustedPrice.toLocaleString('pt-BR', {minimumFractionDigits:2})+'</span></td>'
                    html = html+'</tr>'
                  });
                  html = html+'</table></div><div class="header-cart-items-subtotal">Subtotal: <strong>R$'+data.subTotal.toLocaleString('pt-BR', {minimumFractionDigits:2})+'</strong></div><a href="../../../../../../../../../carrinho" class="header-cart-bt w-100">ir para o carrinho</a>';
                }else{
                  html = html+'<div class="header-cart-items-empty">Seu carrinho est� vazio</div>'
                }
                html = html+'</div>';
          
                $("#header-miniCart").html(html);
                $("#header-cart-quantity").html(cartQuantity+" itens");
              }
            });
        }
        
        $(document).bind('atualizaCarrinhoTopo', function () {
            loadMiniCart();
        });
          
        $(function(){
            $(document).trigger('atualizaCarrinhoTopo'); 
        });
      </script>
    
      <div data-element="content">
            <ciac:scriptrazor id="Catalog" runat="server"></ciac:scriptrazor>   
      </div>
        
    </div>
	<ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
</form>
</body>
</html>