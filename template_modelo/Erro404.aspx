<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Erro404.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Erro404" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
    <title>Divinit� Nutri Cosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
  
    <script src="js/jquery.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>

	<ciac:seo id="seo" runat="server"></ciac:seo>
    <cial:webanalytics id="include_webanalytics" runat="server"></cial:webanalytics>
</head>
<body class="main deslogado">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="header">
        <cial:header id="include_header" runat="server"></cial:header>
    </header>
	
	<main class="box-content">
      <div class="sorry">
          <div class="sorry-content ops">
              <div class="sorry-info">
                  <h2>Ops...</h2>
                  <h3>O que aconteceu?</h3>
                  <p>A p�gina que voc� procura n�o foi encontrada :(</p>
				   <p>Por favor, retorne para a home clicando no bot�o abaixo ;)</p>
                  <br>
				  <a href="~/" class="bt-medium bt-quaternary">Ir para P�gina Inicial</a>
              </div>
          </div>
       </div>      
  	</main>
  
    <footer id="footer">
        <cial:footer id="include_footer" runat="server"></cial:footer>
    </footer>

	<ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
</form>
</body>
</html>