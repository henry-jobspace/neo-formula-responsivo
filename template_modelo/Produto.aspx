<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="Produto.aspx.cs" Inherits="CiaShop.Loja.Assets.Templates._1.Produto" EnableEventValidation="false" UICulture="auto" Culture="auto" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="CiaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><%@ Register Src="ascx/browsinghistory.ascx" TagName="browsinghistory" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
    <title>Divinit� Nutri Cosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
  
    <script src="js/jquery.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>
	<ciac:seo id="seo" runat="server"></ciac:seo>
    <cial:webanalytics id="include_webanalytics" runat="server"></cial:webanalytics>
  	<ciac:widget id="metaTag_og" runat="server"></ciac:widget>
</head>
<body class="main deslogado">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="header">
        <cial:header id="include_header" runat="server"></cial:header>
    </header>  

    <div class="breadCrumb">
      <div class="box-content">
      	<ciac:produtobarranavegacao id="breadCrumb" runat="server"></ciac:produtobarranavegacao>
      </div>
    </div>  
  
    <main class="box-content">
      <div class="product-content">         
        <div class="product-top" itemscope="" itemtype="http://schema.org/Product">
          <div class="hide m--block">
            <div class="product-image-owl"></div>
          </div>
          <div class="js--product-top-left to-left m--to-none m--w-100">
            <ciac:imagensproduto id="product_images" runat="server"></ciac:imagensproduto>
          </div>
          <div class="js--product-top-right align-left m--align-center to-right m--to-none m--w-100">
            <div class="product-top-right-content">
              <a href=".container" class="link-ratingProduct smoothScroll"></a>
              <ciac:produtonome id="product_name" runat="server"></ciac:produtonome>
              <div class="avaliar-seals">
              	<ciac:produtoavaliar id="ProdutoAvaliar1" runat="server"></ciac:produtoavaliar>
              	<ciac:selosproduto id="product_promotions" runat="server"></ciac:selosproduto>
              </div>
              
              <ciac:widget id="shortDescription" runat="server"></ciac:widget>
              
              <div class="product-price-container">
                <div class="product_variants"></div>
                <div class="product-price-box">
                  <ciac:precoproduto id="product_price" runat="server"></ciac:precoproduto>
                  <div class="product-buy">
                    <ciac:variantesproduto id="product_variants_goCheckout" runat="server"></ciac:variantesproduto>
                  </div>
                  
                  <div class="kits">
                    <h3>Itens que comp�em o kit</h3>
                    <ul class="kits_itens"></ul>
                  </div>                  
                  
                  <ciac:simularfreteproduto id="product_shipping" runat="server"></ciac:simularfreteproduto>                  
                </div>
              </div>
              
              <div class="product-share">
                <h3 class="product-heading">Compartilhe com os amigos</h3>
                <a href="javascript: void(0)" class="fb-share" onclick="window.open('http://www.facebook.com/share.php?u=https://www.divinitenutricosmeticos.com.br','Janela','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=400,left=500,top=100'); return false;">
                  <svg class="sprite-svg icon-facebook" role="img"><use xlink:href="imagens/sprite.svg#icon-facebook"></use></svg>
                </a>
                <a href="javascript: void(0)" class="twitter-share">
                  <svg class="sprite-svg icon-twitter" role="img"><use xlink:href="imagens/sprite.svg#icon-twitter"></use></svg>
                </a>
                <a href="javascript: void(0)" class="whatsapp-share">
                  <svg class="sprite-svg icon-whatsapp" role="img"><use xlink:href="imagens/sprite.svg#icon-whatsapp"></use></svg>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  
    <main class="box-content">  
      <div class="product-content box-content">
        <div class="product-bottom container">
          <ciac:relatedproducts id="ProdutosRelacionadosRazor" runat="server"></ciac:relatedproducts>
         
          <div id="descricao">
            <ciac:produtodescricoes id="product_descriptions" runat="server"></ciac:produtodescricoes>
          </div>            
          
          <div class="comentarios_avaliar">
            <div class="tit_box comentarios_avaliar_titulo">

            </div>
            <div class="comentarios_avaliar_conteudo">
              <div class="comentarios_avaliar_abas m--scroll-x">
                <ul class="heading-secondary">
                  <li class="aba_comentarios"><a href="#link_comentarios">Coment�rios</a></li>                                                      
                  <li class="aba_avaliar"><a href="#link_avaliar">Comente sobre este produto</a></li>                                                        
                </ul>
              </div>
              <div class="avaliar conteudo_coment_avaliar" id="link_avaliar">
                <ciac:produtoavaliar id="ProdutoAvaliar2" runat="server"></ciac:produtoavaliar>
              </div>                                              
              <div class="comentarios conteudo_coment_avaliar" id="link_comentarios">
                <ciac:produtocomentario id="ProdutoComentario1" runat="server"></ciac:produtocomentario>
              </div>
            </div>
          </div>
        </div>   
      </div>
    </main>

    <footer id="footer">
        <div class="box-content align-right m--align-center">
          <a href="#header" class="smoothScroll bt-toTop"><svg class="sprite-svg icon-toTop" role="img">
            <use xlink:href="imagens/sprite.svg#icon-up"></use></svg>
          </a>
      	</div>
        <cial:footer id="include_footer" runat="server"></cial:footer>
    </footer>
  
    <ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>  
  
    <div class="ajaxLoader"></div>
  
<script>
$(document).ready(function () {
	var box_description_title = $('.kits_itens');
	$.ajax({
    	url: '/public_api/v1/products/' + produtoId,
    	dataType: 'json',
    	success: function (response) {              
        
        	if(response.type == "normal"){
        		$('.kits').hide();
        	}

        	$.each(response.kitItems, function (i, val) {
    			box_description_title.append("<li><a href='" + val.url + "'><img src='"+ val.mainImage+"' /><p>" + val.name + "<small>Quantidade: " + val.quantity + "</small></p></a></li>");
        	});

	        if (response.mainVariant.quantity > 0) {
    	        $('.add-variations').show();
        	} else {
	            $('.btn-indisponivel').show();
    	    }
    	}
	});

});
</script>

/* Inverte nome com descri��o curta */  
<!--script>
$(document).ready(function() {
  	var aux = $(".desCurta").html();
  	$(".desCurta").html($(".product-name").html());
	$(".product-name").html(aux);  
});
</script-->
</form>
</body>
  
</html> 