<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Confirmado.aspx.cs" MaintainScrollPositionOnPostback="true" Inherits="CiaShop.Loja.Assets.Templates._1.Confirmado" %><%@ Register Assembly="Ciashop.Privada.Componente" Namespace="Ciashop.Privada.Componente.Loja" TagPrefix="ciaC" %><%@ Register Src="ascx/webanalytics.ascx" TagName="webanalytics" TagPrefix="cial" %><%@ Register Src="ascx/header.ascx" TagName="header" TagPrefix="cial" %><%@ Register Src="ascx/footer.ascx" TagName="footer" TagPrefix="cial" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" class="stm-site-preloader">
<head runat="server">
    <title>Divinit� NutriCosm�ticos</title>
    <link rel="shortcut icon" href="imagens/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width">
  
    <script src="js/jquery.1.7.js" type="text/javascript"></script>
      
    <ciac:scriptrazor id="staticFilesHead" runat="server"></ciac:scriptrazor>

  	<ciac:widget id="datalayer_confirm" runat="server"></ciac:widget>
	<ciac:seo id="seo" runat="server"></ciac:seo>
    <cial:webanalytics id="include_webanalytics" runat="server"></cial:webanalytics>
</head>
<body class="main deslogado">
<form id="form2" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<header id="header">
        <cial:header id="include_header" runat="server"></cial:header>
    </header>

    <div class="banner banner-full">
        <ciac:banner id="header_full" runat="server"></ciac:banner>
    </div>

    <main class="box-content">
        <div class="heading-quinary">
          <h3>Pedido Confirmado</h3>
        </div>      
      
        <ciac:alterarsenha id="AlterarSenha" runat="server"></ciac:alterarsenha>
        <div class="orders order-confirmation">
            <div class="to-left w-68 m--w-100">
                <div class="order-info">
                    <div class="order-info-content">
                        <ciac:pedidodados id="order_info" runat="server"></ciac:pedidodados>
                        <ciac:pedidopagamento id="order_payment" runat="server"></ciac:pedidopagamento>
                        <ciac:pedidoentrega id="order_shipping" runat="server"></ciac:pedidoentrega>
                    </div>
                </div>
            </div>
            <div class="to-right w-30 m--w-100">
                <ciac:pedidoitens id="order_items" runat="server"></ciac:pedidoitens>
            </div> 
        </div>
        <div class="go-back-bt align-left">
            <a href="~/" class="bt-small bt-quaternary">Voltar para a p�gina inicial</a>
        </div>
    </main>

    <footer id="footer">
        <cial:footer id="include_footer" runat="server"></cial:footer>
    </footer>
	
    <ciac:scriptrazor id="staticFilesBody" runat="server"></ciac:scriptrazor>
    <div class="ajaxLoader"></div>
</form>
</body>
</html> 