function openPaymentMethod(){
    var methodChecked = $("[name='PaymentMethod.Id']:checked").val();
    try{
        $(".paymentMethod").hide();
        $(".paymentSelecione").hide();
        $("[method="+methodChecked+"]").show();
        $(".paymentMethodNote").show();
    } catch(ex){
        console.log(ex);
    }
    return methodChecked;
}

$(function(){

    if($("[name='PaymentMethod.Id']:checked").val()!=null){
        openPaymentMethod();
    };

    $(".paymentMethodsList-method").click(function(){
        $(this).find("input").prop("checked",true);
        openPaymentMethod();
    });
}); 

/*Sobreescrever funções*/

function AddError(input) {
    if(input.attr("name")=="PaymentInstallments.Quantity"){
        alert("Selecione o número de parcelas");
        input.parents(".parent-validation").addClass("parent-validation-error");
        input.addClass('input-validation-error');
        input.parent().parent().addClass('error');
        if($('#ExpirationMonth option:selected').val() == "Selecione"){
            $('.ExpirationMonth').addClass('error');        
        }
        if($('#ExpirationYear option:selected').val() == "Selecione"){
              $('.ExpirationYear').addClass('error');       
        }
    }else{
        input.parents(".parent-validation").addClass("parent-validation-error");
        input.addClass('input-validation-error');
        input.parent().parent().addClass('error');
        if($('#ExpirationMonth option:selected').val() == "Selecione"){
            $('.ExpirationMonth').addClass('error');        
        }
        if($('#ExpirationYear option:selected').val() == "Selecione"){
              $('.ExpirationYear').addClass('error');       
        }
    }
} 

function RemoveError(input) {
    input.parents(".parent-validation").removeClass("parent-validation-error");
    input.removeClass('input-validation-error');
    input.parent().parent().removeClass('error');
        if($('#ExpirationMonth option:selected').val() != "Selecione"){
            $('.ExpirationMonth').removeClass('error');     
        }
        if($('#ExpirationYear option:selected').val() != "Selecione"){
              $('.ExpirationYear').removeClass('error');        
        }
}