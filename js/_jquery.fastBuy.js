function AbrirModalEspiar(e, t, n) {
    if (document_ready_listagem) {
        var a = e.replace("https:", "").replace("http:", "") + "Espiar.aspx?produto_id=" + t + "&source=es";
        0 != n && (a += "&template_id=" + n)
    } else document_ready_listagem_ultima_acao = function() {
        AbrirModalEspiar(e, t, n)
    }
}

function AbrirModalOverlay(e, t, n) {
    try {
        $(".overlay_Box").remove(), $("body").append("<div class='overlay_Box' style='position:fixed; height: 100%; width:100%; background-color:#000; z-index:9999; top:0; left:0'></div>"), $(".overlay_Box").css("opacity", "0.6").click(function() {
            $(".fecharModal").click()
        }), t && parseInt(t) && $("#modalEspiar").css("width", t + "px"), n && parseInt(n) && $("#modalEspiar").height(n);
        var a = $(e).html();
        $("#modalEspiar").html(a), $("#modalEspiar").append('<a class="fecharModal" href="javascript:void(0)"></a>').css({
            marginLeft: -$("#modalEspiar").width() / 2,
            marginTop: -$("#modalEspiar").height() / 2,
            left: "50%",
            top: "45%"
        }).fadeIn(500)
    } catch (i) {
        $(".fecharModal").click()
    }
}



function removeProduct(taskId) {
    $.ajax({
        type: 'DELETE',
        url: "//" + window.location.hostname + "/public_api/v1/carts/items/" + taskId,
        success: function(t) {
            $(document).trigger('atualizaCarrinhoTopo');

            swal({
                icon: "warning",
                text: "Produto removido do carrinho",
                button: false,
                timer: 1500
            })  
        }
    })
}

function loadMiniCart() {
    var e = 0;
    $.ajax({
        url: "//" + window.location.hostname + "/public_api/v1/carts/minicart",
        dataType: "json",
        cache: !1,
        xhrFields: {
            withCredentials: !0
        },
        success: function(t) {
            var totalCart = 0;
            html = '<div class="header-cart-items">';
                html += '<p class="header-cart-items-title"><strong>Meu Carrinho</strong></p>';            
                if(t.items.length > 0){
                    e = t.items.length;
                    html += '<div class="header-cart-items-list">';
                    html += '<div class="header-cart-products">';

                    $.each(t.items, function(e, t) {
                        if(t.price == 0){
                            html += '<div class="header-cart-item header-card-item-brinde">';
                            html += '<p>Parabés, você ganhou o brinde abaixo:</p>';
                        }else{
                            html += '<div class="header-cart-item">';
                        }
                            html += '<div class="header-cart-item-image"><a href="' + t.url + '"><img src="' + t.image + '?width=80&height=80" alt="+item.name+" /></a></div>';
                            html += '<div class="header-cart-item-info">';
                                html += '<div class="header-cart-item-name"><a href="' + t.url + '"><span>' + t.name + "</span></a> </div>";
                                html += '<div class="header-cart-item-id"><small>Qtd: ' + t.quantity + '</small><span onclick="removeProduct(' + t.itemId + ')"><img title="Remover do carrinho" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMTZweCIgaGVpZ2h0PSIxNnB4Ij4KPGc+Cgk8Zz4KCQk8Zz4KCQkJPHBvbHlnb24gcG9pbnRzPSIzNTMuNTc0LDE3Ni41MjYgMzEzLjQ5NiwxNzUuMDU2IDMwNC44MDcsNDEyLjM0IDM0NC44ODUsNDEzLjgwNCAgICAiIGZpbGw9IiNEODAwMjciLz4KCQkJPHJlY3QgeD0iMjM1Ljk0OCIgeT0iMTc1Ljc5MSIgd2lkdGg9IjQwLjEwNCIgaGVpZ2h0PSIyMzcuMjg1IiBmaWxsPSIjRDgwMDI3Ii8+CgkJCTxwb2x5Z29uIHBvaW50cz0iMjA3LjE4Niw0MTIuMzM0IDE5OC40OTcsMTc1LjA0OSAxNTguNDE5LDE3Ni41MiAxNjcuMTA5LDQxMy44MDQgICAgIiBmaWxsPSIjRDgwMDI3Ii8+CgkJCTxwYXRoIGQ9Ik0xNy4zNzksNzYuODY3djQwLjEwNGg0MS43ODlMOTIuMzIsNDkzLjcwNkM5My4yMjksNTA0LjA1OSwxMDEuODk5LDUxMiwxMTIuMjkyLDUxMmgyODYuNzQgICAgIGMxMC4zOTQsMCwxOS4wNy03Ljk0NywxOS45NzItMTguMzAxbDMzLjE1My0zNzYuNzI4aDQyLjQ2NFY3Ni44NjdIMTcuMzc5eiBNMzgwLjY2NSw0NzEuODk2SDEzMC42NTRMOTkuNDI2LDExNi45NzFoMzEyLjQ3NCAgICAgTDM4MC42NjUsNDcxLjg5NnoiIGZpbGw9IiNEODAwMjciLz4KCQk8L2c+Cgk8L2c+CjwvZz4KPGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMzIxLjUwNCwwSDE5MC40OTZjLTE4LjQyOCwwLTMzLjQyLDE0Ljk5Mi0zMy40MiwzMy40MnY2My40OTloNDAuMTA0VjQwLjEwNGgxMTcuNjR2NTYuODE1aDQwLjEwNFYzMy40MiAgICBDMzU0LjkyNCwxNC45OTIsMzM5LjkzMiwwLDMyMS41MDQsMHoiIGZpbGw9IiNEODAwMjciLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K"></span></div>';                                
                                html += '<div class="header-cart-item-price"><span>R$ ' + t.adjustedPrice.toLocaleString("pt-BR", { minimumFractionDigits: 2 }) + "</span></div>";
                            html += '</div>';
                        html += "</div>";
                        totalCart += t.quantity;

                    })

                    html += '</div>';
                    html += '</div>';
                    html += '<div class="header-cart-items-subtotal">Subtotal: <strong>R$' + t.subTotal.toLocaleString("pt-BR", { minimumFractionDigits: 2 }) + '</strong></div><a href="../../../../../../../../../carrinho" class="bt-medium bt-goCheckout w-100">Ir para o carrinho</a>';
                }else{
                    html += '<div class="header-cart-items-empty"><strong>Carrinho Vazio :(</strong><p>Inserir produtos no carrinho é muito fácil :)</p><span>Navegue pelos departamentos ou use a busca do site</span><span>Ao encontrar o que deseja, clique no botão "<strong>Comprar</strong>"</span></div>';                }
            html += "</div>";
            $("#header-miniCart").html(html);
            $("#header-cart-quantity").text(totalCart);
        }
    })
}

$(document).bind("atualizaCarrinhoTopo", function() {
    loadMiniCart()
})

$(document).on("click", ".js--cart-slider", function(e) {
    overlayIn("show-cart-modal");
})

$(document).on("click", ".show-cart-modal .overlay, .show-cart-modal .header-cart-items-close", function(){
    overlayOut("show-cart-modal");
});