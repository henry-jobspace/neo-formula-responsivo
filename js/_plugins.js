function overlayIn(classIncluded){
    $(".bt-toTop").removeClass('active');
    $("body").addClass('show-overlay');

    $("body").addClass(classIncluded);

    $("body").addClass("noScroll");
}

function overlayOut(classRemoved){
    $(".bt-toTop").addClass('active');
    $("body").removeClass('show-overlay');

    $("body").removeClass(classRemoved);

    $("body").removeClass("noScroll");
}

function LoadZoom() {
    $(".jqzoom").jqzoom({
        zoomType: "innerzoom",
        lens: !0,
        title: !1,
        preloadImages: !1,
        alwaysOn: !1,
        xOffset: "110%",
        preloadText: "Carregando..."
    })
}

function onlyNumber(e) {
    for (keyCodesAllowed = new Array(8, 9, 37, 39, 46), x = 48; x <= 57; x++) keyCodesAllowed.push(x);
    for (x = 96; x <= 105; x++) keyCodesAllowed.push(x);
    return keyCode = e.which, -1 != $.inArray(keyCode, keyCodesAllowed) ? !0 : !1
}

function moneyToNumber(e) {
    return isNaN(e) == false ? parseFloat(e) :   parseFloat(e.replace("R$","").replace(".","").replace(",","."));
}

function numberToMoney(n, c, d, t){
    var c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return "R$ " + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function asMoney(e) {
    return isNaN(e) ? void console.warn("asMoney() - Não foi enviado um valor númerico") : (money = "R$ " + e.toFixed(2).toString().replace(".", ","), money)
}

(function($) {

  $.fn.unveil = function(threshold, callback) {

    var $w = $(window),
        th = threshold || 0,
        retina = window.devicePixelRatio > 1,
        attrib = retina? "data-src-retina" : "data-src",
        images = this,
        loaded;

    this.one("unveil", function() {
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-src");
      if (source) {
        this.setAttribute("src", source);
        if (typeof callback === "function") callback.call(this);
      }
    });

    function unveil() {
      var inview = images.filter(function() {
        var $e = $(this);
        if ($e.is(":hidden")) return;

        var wt = $w.scrollTop(),
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();

        return eb >= wt - th && et <= wb + th;
      });

      loaded = inview.trigger("unveil");
      images = images.not(loaded);
    }

    $w.on("scroll.unveil resize.unveil lookup.unveil", unveil);

    unveil();

    return this;

  };

})(window.jQuery || window.Zepto),


(function( $ ){
    $.fn.upFloatHeader = function(options) {
        var defaults = {
            'startIn' : 190
        };
        var settings = $.extend( {}, defaults, options );
        return this.each (function() {
            var elm = $(this);
            $(window).scroll(function() {
                if ($(window).scrollTop() > elm.height()) {   
                    $(".bt-toTop").addClass('active');
                } else {
                    $(".bt-toTop").removeClass('active');
                }     
            });       
        });
    }; 
})( jQuery ),

function(e) {
    $.fn.upGetProducts = function(t) {
        var n = {
            id: ".loadProducts",
            pagination: ".pages",
            lastPage: ".max-page",
            nextPage: ".next-page"
        }
          , a = $.extend({}, n, t)
          , o = $(this)
          , i = String(window.location).toLowerCase();
        return this.each(function() {
            $(window).scroll(function() {
                $(window).scrollTop() < 100 ? $(".toTop").hide() : $(".toTop").hasClass("hasScroll") && $(".toTop").show()
            });
            var t = 1
              , n = "", load_url = "";
            if ($(a.nextPage).html() && (i.indexOf("/pag/") < 0 || i.indexOf("/pag/1") > 0) && (i.indexOf("pag=") < 0 || i.indexOf("pag=1") > 0)) {
                i.indexOf("resultadopesquisa") > 0 ? (t = $(a.lastPage).html().toLowerCase().split("?pag=")[1],
                t = t.split("&")[0],
                n = $(a.nextPage).attr("href").replace(/pag=2&/gi, "") + "&pag=") : (t = $(a.lastPage).html().toLowerCase().split("/pag")[1],
                t = t.split("/")[1],
                n = $(a.nextPage).attr("href").replace(/\/pag\/2/gi, "") + "/pag/");
                var r = 1;
                o.append("<div id='upGetProducts-loading'>Loading</div>"),
                $(a.pagination).hide(),
                $(window).scroll(function() {
                    $(window).scrollTop() + $(window).height() > o.offset().top + o.height() && 1 != o.hasClass("loading") && t > r && (o.addClass("loading"),
                    r++,
                    load_url = n + r,
                    $.get(load_url, function(n) {
                        var i = $(n).find(a.id + " li");
                        o.find(".js--collection-ul").append(i),
                        o.removeClass("loading"),
                        $(".lazy").unveil(200, function() {
                            $(this).load(function() {
                                $(this).removeClass("lazy").addClass("lazyed")
                            })
                        }),
                        $(".toTop").show().addClass("hasScroll"),
                        r == t && $(".upGetMoreProducts").hide()
                    }))
                })
            } else
                $(".upGetMoreProducts").hide()
        })
    }
}(jQuery),

(function( $ ){
    $.fn.upValidate = function(options) {
        var defaults = {
             'errorInvalidName' : 'Digite um nome válido',
             'errorInvalidEmail' : 'Digite um email válido',  
             'errorPasswordDontMatch' : 'As senhas não conferem',
             'errorMinLength':  'O número mínimo de caracteres é',
             'errorMaxLength' : 'O número máximo de caracteres é',
             'errorRequired' : 'Preenchimento obrigatório'
        };
        var settings = $.extend( {}, defaults, options );
        return this.each (function() {
            var elm = $(this);
            elm.addClass("upValidate");
            
            $(this).find(".validate").each(function(){
                $(this).find("input, select, textarea").each(function(){
                    $(this).addClass("upValidate-field");
                    $(this).after("<span class='validate-message' data-field-name='"+$(this).attr("name")+"'></span>")
                });
            });

            function upValidateField(input){
                var required = ((input.parents(".validate").hasClass("required")) ? true : false);
                var type = input.parents(".validate").attr("data-type");
                var typeName = ((type=="name") ? true : false);
                var typeEmail = ((type=="email") ? true : false);
                var typeRePassword = ((type=="repassword") ? true : false);
                var minLength = ((input.parents(".validate").attr("data-min-length")!="undefined") ? input.parents(".validate").attr("data-min-length") : 0);
                var maxLength = ((input.parents(".validate").attr("data-max-length")!="undefined") ? input.parents(".validate").attr("data-max-length") : 100000);
                var error = false

                if(typeName && input.val().indexOf(" ")<1){
                    showError(input, settings.errorInvalidName);  
                    error = true;                  
                }

                if(typeEmail && (input.val().indexOf("@")<1 || input.val().indexOf(".")<1)){
                    showError(input, settings.errorInvalidEmail);  
                    error = true;  
                }

                if(typeRePassword && input.val() != $("[data-type='password']").find("input").val()){
                    showError(input, settings.errorPasswordDontMatch);   
                    error = true;  
                }

                if(input.val().length < minLength){
                    showError(input, settings.errorMinLength+" "+minLength);   
                    error = true;  
                }

                if(input.val().length > maxLength){
                    showError(input, settings.errorMaxLength+" "+maxLength);   
                    error = true;  
                }

                if(required && input.val()==""){
                    showError(input, settings.errorRequired);   
                    error = true;                 
                }

                if(error && input.offset().left>10){
                    return false;
                }else{
                    hideError(input);
                    return true;
                }
            }
        
            function showError(input,msg){
                $("[data-field-name='"+input.attr("name")+"']").html(msg);
                input.addClass("validate-field-error");
            }

            function hideError(input){
                $("[data-field-name='"+input.attr("name")+"']").text();
                input.removeClass("validate-field-error");
            }

            $(".upValidate-field").blur(function(){                  
                upValidateField($(this));
            });

            $(".upValidate-field").hover(function(){   
                if($(this).hasClass("validate-field-error")){
                    $(this).next(".validate-message").show();
                }
            },function(){
                $(this).next(".validate-message").hide(); 
            });

            $(".fakeButtom").off("click").prop("onclick",null);
            $(".fakeButtom").click(function(event){
                event.preventDefault();
                var error = false;
                $(".upValidate-field").each(function(){   
                    if(!upValidateField($(this))){
                        error = true;
                        console.log($(this).attr("name"));
                    }                    
                });
                if(error){
                    $("html, body").animate({ scrollTop: $(".validate-field-error:first").offset().top-150 });
                    return false;
                }else{
                    $(".realButtom").trigger("click");
                }
            });

         });
      }; 
})( jQuery );

/* Compra Rapida */

$(document).ready(function() {
    $(document).on("blur", ".collection-product-buyInput > input, #product-quantity-input", function() {
        if($(this).val() == ""){
            $("#collection-product-quantity, #product-quantity").val("1").show().removeClass("focus");
        }
    });

    $(document).on("change", "#collection-product-quantity, #product-quantity", function() {
        var e = $(this),
            t = e.val();

        if(t == "+15"){
            e.hide();
            e.next("input").show().focus().select();
            e.closest(".collection-product-buyInput").addClass('focus');
            e.closest(".product-buyInput").addClass('focus');
        }else{
            e.next().val(t).trigger("change");
        }
    });

    $(document).on("change", ".collection-product-buyInput input", function() {
        var e = $(this).attr("name");
        $("input[name='" + e + "']").val($(this).val())
    })
});

function AbrirModalComprarDireto(urlDominio, codigo, hasVariante, template) {

        var qtd = document.getElementById('txtQtd_' + codigo).value;

        var randomnumber = Math.floor(Math.random() * 1001);
        var qtd = document.getElementById('txtQtd_' + codigo).value;
        var url = '';
        if (hasVariante) {
            url = urlDominio + 'Espiar/espiar_id/' + codigo + '/produto_id/' + codigo + '/Qtd/' + qtd + '/source/cd';

            if (qtd > 0) {
                if(!isMobile()){
                    $.colorbox({
                        href: url,
                        width: 680,
                        height: 480,
                        onComplete: function(){
                            $.colorbox.resize();
                            $("#product-quantity, #product-quantity-input").val(qtd);
                        }
                    });                    
                }else{
                    $.colorbox({
                        href: url,
                        width: '80%',
                        height: '80%',
                        onComplete: function(){
                            $.colorbox.resize();
                            $("#product-quantity, #product-quantity-input").val(qtd);
                        }
                    }); 
                }
            }
        }
        else {
            url = urlDominio + 'AddCarrinho?CodVar=' + codigo + '&Qtd=' + qtd + '&source=cd&rand=' + randomnumber;

            swal({
                icon: "success",
                text: "Produto adicionado no carrinho.",
                button: false,
                timer: 1500
            })        

            .then(function() {

                if (qtd > 0){
                    $(".ajaxLoader").show();

                    $.ajax({
                        url: url
                    }).done(function() {
                        $(".ajaxLoader").hide();
                        $(document).trigger("atualizaCarrinhoTopo");                
                        setTimeout(function() {
                            overlayIn("show-cart-modal"); 
                        }, 500)
                    })
                }
            });
        }

}

function AbrirModalComprarDiretoProduct(codigo, bt) {
    var randomnumber = Math.floor(1001 * Math.random());
    var qtd = bt.prev().find('input').val();
    var url = "";
    var urlDominio = window.location.origin + "/";
    $.colorbox.close();
    url = urlDominio + "AddCarrinho?CodVar=" + codigo + "&Qtd=" + qtd + "&source=cd&rand=" + randomnumber;
    
    swal({
        icon: "success",
        text: "Produto adicionado no carrinho.",
        button: false,
        timer: 1500
    })        

    .then(function() {

        if (qtd > 0){
            $(".ajaxLoader").show();

            $.ajax({
                url: url
            }).done(function() {
                $(".ajaxLoader").hide();
                $(document).trigger("atualizaCarrinhoTopo");                
                setTimeout(function() {
                    overlayIn("show-cart-modal"); 
                }, 500)
            })
        }
    });
}