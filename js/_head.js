function isMobile() {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

function login(e, t) {
    var n = $("#" + e + " :input[name$='Email']"),
        a = $("#" + e + " :input[name$='Password']");
        
    if (validarLogin(n, a)) {
        var o = {
            email: n.val(),
            password: a.val(),
            userid: readCookie("UID")
        };
        $.ajax({
            type: "POST",
            url: getUrlBase() + "t/" + t + "/login.json/login",
            data: o,
            dataType: "json",
            crossDomain: !0,
            xhrFields: {
                withCredentials: !0
            },
            success: function(e) {
                e.Success === !0 ? setTimeout(function() {
                    location.reload(!0)
                }, 100) : e.Success === !1 && null !== e.Redirect && (window.location.href = e.Redirect)
            }
        })
    }
}

/* Início Modal Login */

function openLogin(){
    if($('.header-login').hasClass('boxLoginOpen')){
        $('.header-login').removeClass('boxLoginOpen');

        overlayOut("show-login-modal");
    }
    else{
        $('.header-login').addClass('boxLoginOpen');
        
        overlayIn("show-login-modal");
    }
}

$(document).on("click", ".openLogin", function(){
    if($('.header-login').hasClass('boxLoginOpen')){
        $('.header-login').removeClass('boxLoginOpen');

        overlayOut("show-login-modal");
    }
    else{
        $('.header-login').addClass('boxLoginOpen');

        overlayIn("show-login-modal");
    }
});

$(document).on("click", ".closeLogin, .show-login-modal .overlay", function(){
  $('.header-login').removeClass('boxLoginOpen');

  overlayOut("show-login-modal");
});

/* Final Modal Login */


$(window).load(function() {

    if($('html').hasClass('stm-site-preloader')){
        $('html').addClass('stm-site-loaded');

        setTimeout(function(){
            $('html').removeClass('stm-site-preloader stm-site-loaded');
        }, 250);
    }

    if(!$('.breadCrumb').hasClass('breadCrumb-loaded')){
        $('.breadCrumb').addClass('breadCrumb-loaded');
    }

    if(!$('.breadCrumb-list').hasClass('breadCrumb-list-loaded')){
        $('.breadCrumb-list').addClass('breadCrumb-list-loaded');
    }

    // $(".header-top, .header-middle").swiperight(function() {
    //     $(".header-menu-fixed").addClass("menuAnimated");
    // });

});