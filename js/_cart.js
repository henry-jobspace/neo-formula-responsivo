function validarPrePost() {
    return $("#cart_SpanCarrinho").hasClass("recalculatingCart") ? (alert("Antes de finalizar a compra aguarde o carrinho ser recalculado"), !1) : !0
}

$("#cart").on("change", ".cart-product-quantity input", function() {
    $("#cart_SpanCarrinho").addClass("recalculatingCart");
    $(this).closest(".item").find(".cart-product-subtotalPrice").html("<div class='recalculatingValue'/>");
    $(".cart-subTotalValue-price, .cart-totalValue-price").html("<div class='recalculatingValue'/>");
});

$("#cart").on("click", ".cart-product-remove", function() {
    $("#cart_SpanCarrinho").addClass("recalculatingCart");
    $(".cart-subTotalValue-price, .cart-totalValue-price").html("<div class='recalculatingValue'/>")
});

$("#cart").on("change", ".cart-product-quantity select", function() {
    var t = $(this),
        a = t.val();
        if(a == "+ de 15"){
            t.next("input").focus().select()   
        }else{
            t.next("input").val(a).trigger("change")   
        }
});

$("#cart").on("focus", ".cart-product-quantity input", function() {
    $(this).attr("oldValue", $(this).val())
});

$("#cart").on("keyup", ".cart-product-quantity input", function() {
    if($(this).val() != $(this).attr("oldValue")){
        $(this).parents(".cart-product-quantity").addClass("focus")
    }else{
     $(this).parents(".cart-product-quantity").removeClass("focus")
    }
});

$("#cart").on("blur", ".cart-product-quantity input", function() {
    $(this).parents(".cart-product-quantity").removeClass("focus")
});

$(document).ready(function(){
    $(document).on("click", ".add-mais-menos .add-mais", function() {
      $(this).closest(".cart-qtd").find('input').val(parseInt($(this).closest(".cart-qtd").find('input').val()) + 1);
      $(this).closest(".cart-qtd").find('input').focus().change();
    });

    $(document).on("click", ".add-mais-menos .add-menos", function() {
        if($(this).closest(".cart-qtd").find('input').val() > 1){
            $(this).closest(".cart-qtd").find('input').val(parseInt($(this).closest(".cart-qtd").find('input').val()) - 1);
            $(this).closest(".cart-qtd").find('input').focus().change();
        }else if($(this).closest(".cart-qtd").find('input') == 1){
            return false;
        }
    });
      
});