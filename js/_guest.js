$("#ShippingAddresses_ZipCode").val() && $(".shippingCalculated").slideDown(), "False" == $("input[name=IsSameBillingAddress]:checked").val() && $(".billing-address").slideDown(), $(".guest").on("change", "#ShippingAddresses_ZipCode", function() {
    return $("#Customer_BillingAddress_ZipCode").val($("#ShippingAddresses_ZipCode").val()), $.ajax({
        url: applicationUrlBase + "api/cep/" + $("#ShippingAddresses_ZipCode").val(),
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        success: function(t) {
            $("#ShippingAddresses_Street").val(t.street), $("#ShippingAddresses_District").val(t.district), $("#ShippingAddresses_City").val(t.city), $("#ShippingAddresses_State").find('option[value="' + t.state + '"]').attr("selected", !0), $(".shippingCalculated").slideDown(), $("#Customer_BillingAddress_Street").val(t.street), $("#Customer_BillingAddress_District").val(t.district), $("#Customer_BillingAddress_City").val(t.city), $("#Customer_BillingAddress_State").find('option[value="' + t.state + '"]').attr("selected", !0), $("form").each(function() {
                $(this).valid()
            })
        },
        error: function() {
            alert("Cep não encontrado, por favor preencha todos os campos corretamente."), $(".shippingCalculated").slideDown()
        }
    }), !1
}), $(".guest").on("change", "#Customer_BillingAddress_ZipCode", function() {
    return $.ajax({
        url: applicationUrlBase + "api/cep/" + $("#Customer_BillingAddress_ZipCode").val(),
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        success: function(t) {
            $("#Customer_BillingAddress_Street").val(t.street), $("#Customer_BillingAddress_District").val(t.district), $("#Customer_BillingAddress_City").val(t.city), $("#Customer_BillingAddress_State").find('option[value="' + t.state + '"]').attr("selected", !0), $("form").each(function() {
                $(this).valid()
            })
        }
    }), !1
}), $(".guest").on("change", "#IsSameBillingAddress", function() {
    "True" == $("input[name=IsSameBillingAddress]:checked").val() ? ($(".billing-address").slideUp(), $("#Customer_BillingAddress_ZipCode").val($("#ShippingAddresses_ZipCode").val()), $("#Customer_BillingAddress_Street").val($("#ShippingAddresses_Street").val()), $("#Customer_BillingAddress_StreetNumber").val($("#ShippingAddresses_StreetNumber").val()), $("#Customer_BillingAddress_District").val($("#ShippingAddresses_District").val()), $("#Customer_BillingAddress_City").val($("#ShippingAddresses_City").val()), $("#Customer_BillingAddress_State").find('option[value="' + $("#ShippingAddresses_State").val() + '"]').attr("selected", !0)) : ($(".billing-address").slideDown(), $("#Customer_BillingAddress_ZipCode").val(""), $("#Customer_BillingAddress_Street").val(""), $("#Customer_BillingAddress_StreetNumber").val(""), $("#Customer_BillingAddress_District").val(""), $("#Customer_BillingAddress_City").val(""), $("#Customer_BillingAddress_State").find('option[value=""]').attr("selected", !0))
}), $(".guest").on("change", "#ShippingAddresses_StreetNumber", function() {
    $("#Customer_BillingAddress_StreetNumber").val($("#ShippingAddresses_StreetNumber").val())
}), $(".guest").on("change", "#ShippingAddresses_Street", function() {
    $("#Customer_BillingAddress_Street").val($("#ShippingAddresses_Street").val())
}), $(".guest").on("change", "#ShippingAddresses_District", function() {
    $("#Customer_BillingAddress_District").val($("#ShippingAddresses_District").val())
}), $(".guest").on("change", "#ShippingAddresses_City", function() {
    $("#Customer_BillingAddress_City").val($("#ShippingAddresses_City").val())
}), $(".guest").on("change", "#ShippingAddresses_State", function() {
    $("#Customer_BillingAddress_State").find('option[value="' + $("#ShippingAddresses_State").val() + '"]').attr("selected", !0)
}), $(".guest").on("change", "#Customer_CustomerType", function() {
    "Person" == this.value ? ($("#Customer_RegionalDocumentId, #Customer_DocumentId, #Customer_Gender").val(""), $("#Customer_RegionalDocumentId").removeAttr("disabled"), $("#chkIsentoInscricaoEstadual").removeAttr("checked"), $(".register-cpf .form-title").text("CPF"), $(".personType-PJ").hide()) : "Company" == this.value && ($("#Customer_RegionalDocumentId, #Customer_DocumentId").val(""), $("#Customer_Gender").val("undefined"), $("#chkIsentoInscricaoEstadual").removeAttr("checked"), $(".register-cpf .form-title").text("CNPJ"), $(".personType-PJ").show())
}), $(".guest").on("change", "#chkIsentoInscricaoEstadual", function() {
    null != $("#chkIsentoInscricaoEstadual").attr("checked") ? ($("#Customer_RegionalDocumentId").attr("readonly", "true"), $("#Customer_RegionalDocumentId").val("ISENTO")) : ($("#Customer_RegionalDocumentId").removeAttr("readonly"), $("#Customer_RegionalDocumentId").val(""))
}), $(document).ready(function() {
    "Company" == $("#Customer_CustomerType:checked").val() ? ($(".personType-PJ").show(), $(".register-cpf .form-title").text("CNPJ")) : $(".personType-PJ").hide()
});