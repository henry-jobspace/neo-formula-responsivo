$(window).load(function() {
    $("body").removeClass("deslogado");

    /* Efeito Banners Home */

        AOS.init();
    
    /* Lazy Imagens */

       $(".lazy").unveil(200);    

    /* Ancora */

        $(".smoothScroll").click(function() {
            return $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top - 10
            }, 800); !1
        });

    /* Filtros */

        $(".js--collection-filter").tap(function() {
            $(".js--aside-slider").addClass("active");
        });

        $(".js--aside-slider-close").tap(function() {
            $(".js--aside-slider").removeClass("active");
        })  

        /* Abrir abas no rodapé */
        $(".js--tab-title").click(function() {
            if(isMobile()){         
                $(this).next().slideToggle();
            }else{
                return false;
            }               
        }); 

    /* Abre Menu - Mobile */
    
        $(".openMenu").tap(function() {
            overlayIn("show-menu-modal");        
        });

        $(".openMenu").swipeleft(function() {
            overlayOut("show-menu-modal");
        });    

    /* Ações de abrir filhos e neto do menu - Mobile */

        $(".js--header-menu-a").click(function() {
            $(this).parent().toggleClass("click-parents")
        });

        $(".js--header-subMenu-a").click(function() {
            $(this).parent().toggleClass("click-parents-subMenu")
        });

        $(".back").tap(function() {
            $(this).parent().parent().parent().removeClass("click-parents");
            $(".box-menu-parents").removeClass("click-box-parents")
        });

        $(".box-menu-children").swiperight(function() {
            $(this).parent().removeClass("click-parents");
            $(".box-menu-parents").removeClass("click-box-parents")
        });

        $(".js--header-menu-li").each(function() {
            $(this).find(".js--box-subMenu-parents").length ? $(this).find(".js--header-menu-a").attr("href", "javascript:void(0);") : $(this).find(".js--header-menu-a-icon").hide()
        });

        $(".js--header-subMenu-li").each(function() {
            $(this).find(".js--box-grandsonMenu-parents").length ? $(this).find(".js--header-subMenu-a").attr("href", "javascript:void(0);") : $(this).find(".js--header-subMenu-a-icon").hide()
        });

        $(".js--header-cart-a").tap(function() {
            $(".js--cart-slider").addClass("active");
        });

        $(".js--cart-slider-close").tap(function() {
            $(".js--cart-slider").removeClass("active");
        })    

        /* Se desconto for 0% nao exibe selo */
        if(parseInt($(".colleciton-product-discountPercentValue").attr("data-value")) == 0){
            $(".collection-product-discountPercent").hide();
        }    

    /* Fecha Menu - Mobile */

    /* Inicio do Modal */        

        $(document).on("click", ".show-menu-modal .overlay", function(){
            overlayOut("show-menu-modal");
        });   

    /* Final do Modal */    

    /* Abas descrição do produto */

        $(function () {
             var tabContainers = $('.descricao_texto .descricao_texto_conteudo');
             $('.descricao .produto_abas li a').click(function () {
                tabContainers.hide().filter(this.hash).show();
                  $('.descricao .produto_abas a').parent().removeClass("heading-tabs");
                  $(this).parent().addClass("heading-tabs");  
                return false;
            }).filter(':first').click();
        }); 

    /* Focus Buscar */

        if(isMobile()){
            $(document).on("focus", ".header-search-input input", function(){
                $(".header-search").addClass("focus");
                overlayIn("show-search-modal");
            });    

            $(document).on("click", ".show-search-modal .overlay", function(){
                $(".header-search").removeClass("focus");
                overlayOut("show-search-modal");
            });  
               
        }
});