$(document).ready(function() {

    /* Compartilhar */
    $(document).ready(function(){
        var e = $("link[rel=canonical]").attr("href");
        t = encodeURIComponent(e).replace(/'/g, "%27").replace(/"/g, "%22");
        text = encodeURIComponent("Na Boutin você encontra: ").replace(/'/g, "%27").replace(/"/g, "%22");

        $(".twitter-share").click(function() {
            var windowOptions = "toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no";
            target2 = "https://twitter.com/intent/tweet?text=" + text + t;
            window.open(target2, "intent", windowOptions + ",width=640 ,height=442, left=500,top=100");
            return false;
        });

        if(isMobile()){
            $(".whatsapp-share").attr("href", "whatsapp://send?text=" + text + e);
        }else{
            $(".whatsapp-share").attr("target", "_blank");
            $(".whatsapp-share").attr("href", "https://api.whatsapp.com/send?l=pt&text=" + text + e);
        }

        $(".fb-share").attr("onclick", "window.open('http://www.facebook.com/share.php?u=" + e + "','Janela','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=400,left=500,top=100'); return false;");
    })

    /* Abas Descrição */

    $(".product-descriptions-title").click(function() {
        var e = $(this).parents(".product-descriptions-item");
        "" == e.find(".product-descriptions-content").html() && e.find(".product-descriptions-content").html($("[data-description-content='" + e.attr("data-description") + "']").html()), e.toggleClass("active")
    })

    /* Avaliação/Comentários */

    $(".product-rating").prepend($(".product-rating-content .avaliacao_estrela").html());

    $(".ir_avaliar").click(function(){
        $("[href='#link_avaliar']").click();
        $('html, body').animate({scrollTop: $('.comentarios_avaliar_abas').offset().top}, 'slow');
        return false;
    });    

    $(function () {
        var tabContainers = $('.conteudo_coment_avaliar');
        $('.comentarios_avaliar_abas li a').click(function () {
            tabContainers.hide().filter(this.hash).show();
            $('.comentarios_avaliar_abas li a').parent().removeClass("heading-tabs");
            $(this).parent().addClass("heading-tabs");
            return false;
        }).filter(':first').click();
    });    

});

function login(id, templateid,xmlComponente,urlComponente) {
    var email = $("#" + id + " :input[name$='Email']");
    var password = $("#" + id + " :input[name$='Password']");
    if (!validarLogin(email, password)) {
        return;
    }
    var values = { email: email.val(), password: password.val(), userid: readCookie("UID") };
    $.ajax({
        type: "POST",
        url: getUrlBase() + "t/" + templateid + "/login.json/login",
        data: values,
        dataType: 'json',
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        },
        success: function (responseBack) {
            if (responseBack.Success === true) {
                $("#" + id + "").html(LoadComponente(xmlComponente, id, urlComponente));
                $(document).trigger('atualizaCarrinhoTopo');
                $(document).trigger('callShowcaseRecommendationForYou');
                $(".closeLogin").trigger('click');

                setTimeout(function(){ $('.relatedProducts .js--collection-ul').slick('reinit'); }, 1000);

            } else if (responseBack.Success === false) {
                if (responseBack.Redirect !== null) {
                    window.location.href = responseBack.Redirect;
                }
            }
        }
    });
}

$(window).load(function() {

    /* Mini Imagens */

    $(".product-image-small .product-image-repeat:first-child img").addClass("active");

    $(".product-image-small img").click(function() {
        $(".product-image-small img").removeClass("active"), $(this).addClass("active")
    });

    var e = $(".product-image-repeat").length;
    var t = 0;

    if(e > 3){
        $(".product-image-small a").show();
        $(".product-image-small-content").attr("data-images-total", e)
    }

    $(".js--product-image-slideUp").click(function() {
        if(t > 0){
            t -= 1;
            $(".product-image-small-content").attr("data-image-position", t);
            $(".product-image-repeat img").removeClass("active");
            $(".product-image-repeat:nth-child(" + (t + 1) + ") img").trigger("click");
            $(".js--product-image-small-slider").css("transform", "translateY(" + -62 * t + "px)")
        }
    });

    $(".js--product-image-slideDown").click(function() {
        if(e - 3 > t){
            t += 1;
            $(".product-image-small-content").attr("data-image-position", t);
            $(".product-image-repeat img").removeClass("active");
            $(".product-image-repeat:nth-child(" + (t + 1) + ") img").trigger("click");
            $(".js--product-image-small-slider").css("transform", "translateY(" + -62 * t + "px)")
        }
    })

    $(".product-image-big img").attr("src", $(".product-image-big img").attr("src").replace("Gigantes", "SuperZoom"));

    $(".product-image-repeat img").each(function() {
        $(this).attr("onclick", $(this).attr("onclick").replace("Gigantes", "SuperZoom"))
    });

    /* Carousel no Mobile */

    if(isMobile()){
        if($(".product-image-repeat").length >= 1){
            var htmlImages = "";
            $(".product-image-repeat img").each(function(){
                htmlImages = htmlImages + ($(this).parent().html().replace("/Pequenas/","/Gigantes/"));
            });
        
            $(".product-image-owl").html(htmlImages);

            $('.product-image-owl').slick({
                infinite: true,
                dots: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                autoplay: false    
            });
        }else{
            $(".product-image-big").show();
        }
    }
    
});